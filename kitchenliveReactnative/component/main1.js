"use strict";

import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  TextInput,
  ListView,
  Platform
} from "react-native";

import {
  RTCPeerConnection,
  RTCMediaStream,
  RTCIceCandidate,
  RTCSessionDescription,
  RTCView,
  MediaStreamTrack,
  getUserMedia
} from "react-native-webrtc";

import ExtraDimensions from "react-native-extra-dimensions-android";
import InCallManager from "react-native-incall-manager";

var Dimensions = require("Dimensions");
var window = Dimensions.get("window");

var echotest = null;
var started = false;

// var server = "192.168.1.65"
// var server = 'ws://192.168.1.65:8188/'; //JANUS
var server = "ws://192.168.1.65:8188"; //JANUS

var janus = null;
var sfutest = null;
var started = false;

var myusername = null;
var myid = null;
var mystream = null;

var feeds = [];
var bitrateTimer = [];

var localstream_janus, remotestream_janus;

var Janus = require("./janus.nojquery.js");

// $(document).ready(function() {
// Initialize the library (all console debuggers enabled)

// janus = new Janus()
function checkEnter(field, event) {
  var theCode = event.keyCode
    ? event.keyCode
    : event.which ? event.which : event.charCode;
  if (theCode == 13) {
    registerUsername();
    return false;
  } else {
    return true;
  }
}

function publishOwnFeed(useAudio) {
  sfutest.createOffer({
    media: {
      audioRecv: false,
      videoRecv: false,
      audioSend: useAudio,
      videoSend: true
    }, // Publishers are sendonly
    success(jsep) {
      Janus.debug("Got publisher SDP!");
      Janus.debug(jsep);
      var publish = { request: "configure", audio: useAudio, video: true };
      sfutest.send({ message: publish, jsep });
    },
    error(error) {
      Janus.error("WebRTC error:", error);
      if (useAudio) {
        publishOwnFeed(false);
      } else {
        bootbox.alert(`WebRTC error... ${JSON.stringify(error)}`);
        $("#publish")
          .removeAttr("disabled")
          .click(function() {
            publishOwnFeed(true);
          });
      }
    }
  });

  // sfutest.createOffer({
  // 		// Add data:true here if you want to publish datachannels as well
  // 		media: { audioRecv: false, videoRecv: false, audioSend: useAudio, videoSend: true },	// Publishers are sendonly
  // 		// If you want to test simulcasting (Chrome and Firefox only), then
  // 		// pass a ?simulcast=true when opening this demo page: it will turn
  // 		// the following 'simulcast' property to pass to janus.js to true
  // 		simulcast: doSimulcast,
  // 		success: function(jsep) {
  // 			Janus.debug("Got publisher SDP!");
  // 			Janus.debug(jsep);
  // 			var publish = { "request": "configure", "audio": useAudio, "video": true };
  // 			sfutest.send({"message": publish, "jsep": jsep});
  // 		},
  // 		error: function(error) {
  // 			Janus.error("WebRTC error:", error);
  // 			if (useAudio) {
  // 				 publishOwnFeed(false);
  // 			} else {
  // 				bootbox.alert("WebRTC error... " + JSON.stringify(error));
  // 				$('#publish').removeAttr('disabled').click(function() { publishOwnFeed(true); });
  // 			}
  // 		}
  // 	});
}

function toggleMute() {
  var muted = sfutest.isAudioMuted();
  Janus.log(`${muted ? "Unmuting" : "Muting"} local stream...`);
  if (muted) sfutest.unmuteAudio();
  else sfutest.muteAudio();
  muted = sfutest.isAudioMuted();
  $("#mute").html(muted ? "Unmute" : "Mute");
}

function unpublishOwnFeed() {
  // Unpublish our stream
  $("#unpublish")
    .attr("disabled", true)
    .unbind("click");
  var unpublish = { request: "unpublish" };
  sfutest.send({ message: unpublish });
}

function newRemoteFeed(id, display) {
  // A new feed has been published, create a new plugin handle and attach to it as a listener
  var remoteFeed = null;
  janus.attach({
    plugin: "janus.plugin.videoroom",
    success(pluginHandle) {
      remoteFeed = pluginHandle;
      Janus.log(
        `Plugin attached! (${remoteFeed.getPlugin()}, id=${remoteFeed.getId()})`
      );
      Janus.log("  -- This is a subscriber");
      // We wait for the plugin to send us an offer1
      var listen = { request: "join", room: 1234, ptype: "listener", feed: id };
      remoteFeed.send({ message: listen });
    },
    error(error) {
      Janus.error("  -- Error attaching plugin...", error);
      bootbox.alert(`Error attaching plugin... ${error}`);
    },
    onmessage(msg, jsep) {
      Janus.debug(" ::: Got a message (listener) :::");
      Janus.debug(JSON.stringify(msg));
      var event = msg.videoroom;
      Janus.debug(`Event: ${event}`);
      if (event != undefined && event != null) {
        if (event === "attached") {
          // Subscriber created and attached
        }
      }
      if (jsep !== undefined && jsep !== null) {
        Janus.debug("Handling SDP as well...");
        Janus.debug(jsep);
        // Answer and attach
        remoteFeed.createAnswer({
          jsep,
          media: { audioSend: false, videoSend: false }, // We want recvonly audio/video
          success(jsep) {
            Janus.debug("Got SDP!");
            Janus.debug(jsep);
            var body = { request: "start", room: 1234 };
            remoteFeed.send({ message: body, jsep });
          },
          error(error) {
            Janus.error("WebRTC error:", error);
            bootbox.alert(`WebRTC error... ${JSON.stringify(error)}`);
          }
        });
      }
    },
    webrtcState(on) {
      Janus.log(
        `Janus says this WebRTC PeerConnection (feed #${remoteFeed.rfindex}) is ${on
          ? "up"
          : "down"} now`
      );
    },
    onlocalstream(stream) {
      // The subscriber stream is recvonly, we don't expect anything here
    },
    onremotestream(stream) {
      console.log("onaddstream", stream);
      container.setState({ info: "One peer join!" });

      const remoteList = container.state.remoteList;
      //       remoteList[remoteFeed.getId()] = stream.toURL();
      //       container.setState({ remoteList });
      container.setState({ remoteTestSrc: stream.toURL() });
    },
    oncleanup() {
      Janus.log(` ::: Got a cleanup notification (remote feed ${id}) :::`);
      if (remoteFeed.spinner !== undefined && remoteFeed.spinner !== null)
        remoteFeed.spinner.stop();
      remoteFeed.spinner = null;
      $(`#waitingvideo${remoteFeed.rfindex}`).remove();
      $(`#curbitrate${remoteFeed.rfindex}`).remove();
      $(`#curres${remoteFeed.rfindex}`).remove();
      if (
        bitrateTimer[remoteFeed.rfindex] !== null &&
        bitrateTimer[remoteFeed.rfindex] !== null
      )
        clearInterval(bitrateTimer[remoteFeed.rfindex]);
      bitrateTimer[remoteFeed.rfindex] = null;
    }
  });
}

const configuration = { iceServers: [{ url: "stun.l.google.com:19302" }] };

const pcPeers = {};
let localStream;

function getLocalStream(isFront, callback) {
  MediaStreamTrack.getSources(sourceInfos => {
    console.log(sourceInfos);
    let videoSourceId;
    for (const i = 0; i < sourceInfos.length; i++) {
      const sourceInfo = sourceInfos[i];
      if (
        sourceInfo.kind == "video" &&
        sourceInfo.facing == (isFront ? "front" : "back")
      ) {
        videoSourceId = sourceInfo.id;
      }
    }
    getUserMedia(
      {
        audio: true,
        video: {
          optional: [{ sourceId: videoSourceId }]
        }
      },
      function(stream) {
        console.log("dddd", stream);
        callback(stream);
      },
      logError
    );
  });
}

function initStream() {
  InCallManager.start({ media: "video" });
  InCallManager.setMicrophoneMute(true);
  // InCallManager.track.enable = false
  // InCallManager.start({media: 'audio'});
  InCallManager.setForceSpeakerphoneOn.enable = true;
  InCallManager.turnScreenOn.enable = true;
  InCallManager.setKeepScreenOn.enable = true;
  // getLocalStream(true, function(stream) {
  //   localStream = stream;
  //   container.setState({ selfViewSrc: stream.toURL() });
  //   container.setState({
  //     status: "ready",
  //     info: "Please enter or create room ID"
  //   });
  // });
}
function logError(error) {
  console.log("logError", error);
}

function mapHash(hash, func) {
  const array = [];
  for (const key in hash) {
    const obj = hash[key];
    array.push(func(obj, key));
  }
  return array;
}

function getStats() {
  const pc = pcPeers[Object.keys(pcPeers)[0]];
  if (
    pc.getRemoteStreams()[0] &&
    pc.getRemoteStreams()[0].getAudioTracks()[0]
  ) {
    const track = pc.getRemoteStreams()[0].getAudioTracks()[0];
    console.log("track", track);
    pc.getStats(
      track,
      function(report) {
        console.log("getStats report", report);
      },
      logError
    );
  }
}

let container;

// export default class First extends Component {
export default class main1 extends Component {
  // const RCTWebRTCDemo = React.createClass({
  getInitialState() {
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => true });
    return {
      info: "Initializing",
      status: "init",
      roomID: "",
      isFront: true,
      selfViewSrc: null,
      remoteList: {},
      textRoomConnected: false,
      textRoomData: [],
      textRoomValue: ""
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      info: "Initializing",
      status: "init",
      isFront: true,
      selfViewSrc: null,
      remoteTestSrc: null,
      remoteViewSrc: null,
      windowWidth: Dimensions.get("window").width,
      windowHeight: Dimensions.get("window").height
    };
  }

  componentDidMount() {
    container = this;
    this.setState({ info: "Initializing" });
    initStream();
    this._orginalConnectJanus();
  }
  _orginalConnectJanus(event) {
    var Janus = require("./janus.nojquery.js");
    Janus.init({
      debug: "all",
      callback() {
        if (started) return;
        started = true;
      }
    });
    janus = new Janus({
      server,
      iceServers: [
        {
          urls: "turn:23.97.53.170:3478",
          username: "ted",
          credential: "park3764"
        }
      ],
      success() {
        janus.attach({
          plugin: "janus.plugin.videoroom",
          success(pluginHandle) {
            sfutest = pluginHandle;
            Janus.log(
              `Plugin attached! (${sfutest.getPlugin()}, id=${sfutest.getId()})`
            );
            Janus.log("  -- This is a publisher/manager");
            // var body = { audio: true, video: true };
            // Janus.debug(`Sending message (${JSON.stringify(body)})`);
            // sfutest.send({ message: body });

            sfutest.createOffer({
              // No media provided: by default, it's sendrecv for audio and video
              media: {
                audioRecv: false,
                videoRecv: false,
                audioSend: true,
                videoSend: true
              }, // Publishers are sendonly
              success(jsep) {
                Janus.debug("Got publisher SDP!");
                Janus.debug(jsep);
                var publish = {
                  request: "configure",
                  audio: true,
                  video: true
                };
                sfutest.send({ message: publish, jsep });
              },
              error(error) {
                Janus.error("WebRTC error:", error);
                bootbox.alert(`WebRTC error... ${JSON.stringify(error)}`);
              }
            });

            var register = {
              request: "join",
              room: 1234,
              ptype: "publisher",
              display: "username"
            };
            sfutest.send({ message: register });
            console.log("send msg join room");
          },
          error(error) {
            Janus.error("  -- Error attaching plugin...", error);
            bootbox.alert(`Error attaching plugin... ${error}`);
          },
          consentDialog(on) {},
          mediaState(medium, on) {
            Janus.log(
              `Janus ${on ? "started" : "stopped"} receiving our a ${medium}`
            );
          },
          webrtcState(on) {
            Janus.log(
              `Janus says our WebRTC PeerConnection is ${on
                ? "up"
                : "down"} now`
            );
            // $("#videolocal").parent().parent().unblock();
          },
          onmessage(msg, jsep) {
            Janus.debug(" ::: Got a message (publisher) :::");
            Janus.debug(JSON.stringify(msg));
            var event = msg.videoroom;
            Janus.debug(`Event: ${event}`);
            if (event != undefined && event != null) {
              if (event === "joined") {
                // Publisher/manager created, negotiate WebRTC and attach to existing feeds, if any
                myid = msg.id;
                Janus.log(
                  `Successfully joined room ${msg.room} with ID ${myid}`
                );
                publishOwnFeed(true);
                // Any new feed to attach to?
                if (msg.publishers !== undefined && msg.publishers !== null) {
                  var list = msg.publishers;
                  Janus.debug("Got a list of available publishers/feeds:");
                  Janus.debug(list);
                  for (var f in list) {
                    var id = list[f].id;
                    var display = list[f].display;
                    Janus.debug(`  >> [${id}] ${display}`);
                    newRemoteFeed(id, display);
                  }
                }
              } else if (event === "destroyed") {
                // The room has been destroyed
                Janus.warn("The room has been destroyed!");
                bootbox.alert(error, function() {
                  window.location.reload();
                });
              } else if (event === "event") {
                // Any new feed to attach to?
                if (msg.publishers !== undefined && msg.publishers !== null) {
                  var list = msg.publishers;
                  Janus.debug("Got a list of available publishers/feeds:");
                  Janus.debug(list);
                  for (var f in list) {
                    var id = list[f].id;
                    var display = list[f].display;
                    Janus.debug(`  >> [${id}] ${display}`);
                    newRemoteFeed(id, display);
                  }
                } else if (msg.leaving !== undefined && msg.leaving !== null) {
                  // One of the publishers has gone away?
                  var leaving = msg.leaving;
                  Janus.log(`Publisher left: ${leaving}`);
                  var remoteFeed = null;
                  for (var i = 1; i < 6; i++) {
                    if (
                      feeds[i] != null &&
                      feeds[i] != undefined &&
                      feeds[i].rfid == leaving
                    ) {
                      remoteFeed = feeds[i];
                      break;
                    }
                  }
                  if (remoteFeed != null) {
                    Janus.debug(
                      `Feed ${remoteFeed.rfid} (${remoteFeed.rfdisplay}) has left the room, detaching`
                    );
                    $(`#remote${remoteFeed.rfindex}`)
                      .empty()
                      .hide();
                    $(`#videoremote${remoteFeed.rfindex}`).empty();
                    feeds[remoteFeed.rfindex] = null;
                    remoteFeed.detach();
                  }
                } else if (
                  msg.unpublished !== undefined &&
                  msg.unpublished !== null
                ) {
                  // One of the publishers has unpublished?
                  var unpublished = msg.unpublished;
                  Janus.log(`Publisher left: ${unpublished}`);
                  if (unpublished === "ok") {
                    // That's us
                    sfutest.hangup();
                    return;
                  }
                  var remoteFeed = null;
                  for (var i = 1; i < 6; i++) {
                    if (
                      feeds[i] != null &&
                      feeds[i] != undefined &&
                      feeds[i].rfid == unpublished
                    ) {
                      remoteFeed = feeds[i];
                      break;
                    }
                  }
                  if (remoteFeed != null) {
                    Janus.debug(
                      `Feed ${remoteFeed.rfid} (${remoteFeed.rfdisplay}) has left the room, detaching`
                    );
                    $(`#remote${remoteFeed.rfindex}`)
                      .empty()
                      .hide();
                    $(`#videoremote${remoteFeed.rfindex}`).empty();
                    feeds[remoteFeed.rfindex] = null;
                    remoteFeed.detach();
                  }
                } else if (msg.error !== undefined && msg.error !== null) {
                  bootbox.alert(msg.error);
                }
              }
            }
            if (jsep !== undefined && jsep !== null) {
              Janus.debug("Handling SDP as well...");
              Janus.debug(jsep);
              sfutest.handleRemoteJsep({ jsep });
            }
          },
          onlocalstream(stream) {
            localstream_janus = stream;
            Janus.debug(stream);
            // Janus.attachMediaStream($('#myvideo').get(0), stream);
            // var videoTracks = stream.getVideoTracks();
            container.setState({ selfViewSrc: stream.toURL() });
            container.setState({
              status: "ready",
              info: "Please enter or create room ID"
            });
          },
          onremotestream(stream) {
            // The publisher stream is sendonly, we don't expect anything here
          },
          oncleanup() {
            Janus.log(
              " ::: Got a cleanup notification: we are unpublished now :::"
            );
            mystream = null;
            $("#videolocal").html(
              '<button id="publish" class="btn btn-primary">Publish</button>'
            );
            $("#publish").click(function() {
              publishOwnFeed(true);
            });
            $("#videolocal")
              .parent()
              .parent()
              .unblock();
          }
        });
      },
      error(error) {
        // Janus.error(error);
        // bootbox.alert(error, function() {
        //     window.location.reload();
        // });
      },
      destroyed() {
        window.location.reload();
      }
    });
  }

  _press(event) {
    this.refs.roomID.blur();
    this.setState({ status: "connect", info: "Connecting" });
    join(this.state.roomID);
  }
  _switchVideoType() {
    const isFront = !this.state.isFront;
    this.setState({ isFront });
    getLocalStream(isFront, function(stream) {
      if (localStream) {
        for (const id in pcPeers) {
          const pc = pcPeers[id];
          pc && pc.removeStream(localStream);
        }
        localStream.release();
      }
      localStream = stream;
      container.setState({ selfViewSrc: stream.toURL() });

      for (const id in pcPeers) {
        const pc = pcPeers[id];
        pc && pc.addStream(localStream);
      }
    });
  }
  receiveTextData(data) {
    const textRoomData = this.state.textRoomData.slice();
    textRoomData.push(data);
    this.setState({ textRoomData, textRoomValue: "" });
  }
  _textRoomPress() {
    if (!this.state.textRoomValue) {
      return;
    }
    const textRoomData = this.state.textRoomData.slice();
    textRoomData.push({ user: "Me", message: this.state.textRoomValue });
    for (const key in pcPeers) {
      const pc = pcPeers[key];
      pc.textDataChannel.send(this.state.textRoomValue);
    }
    this.setState({ textRoomData, textRoomValue: "" });
  }
  _renderTextRoom() {
    return (
      <View style={styles.listViewContainer}>
        <ListView
          dataSource={this.ds.cloneWithRows(this.state.textRoomData)}
          renderRow={rowData => (
            <Text>{`${rowData.user}: ${rowData.message}`}</Text>
          )}
        />
        <TextInput
          style={{
            width: 200,
            height: 30,
            borderColor: "gray",
            borderWidth: 1
          }}
          onChangeText={value => this.setState({ textRoomValue: value })}
          value={this.state.textRoomValue}
        />
        <TouchableHighlight onPress={this._textRoomPress}>
          <Text>Send</Text>
        </TouchableHighlight>
      </View>
    );
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>{this.state.info}</Text>
        {this.state.textRoomConnected && this._renderTextRoom()}
        <View style={{ flexDirection: "row" }}>
          <Text>
            {this.state.isFront ? "Use front camera" : "Use back camera"}
          </Text>
          <TouchableHighlight
            style={{ borderWidth: 1, borderColor: "black" }}
            onPress={this._switchVideoType}
          >
            <Text>Switch camera</Text>
          </TouchableHighlight>
        </View>
        {this.state.status == "ready" ? (
          <View>
            <TextInput
              ref="roomID"
              autoCorrect={false}
              style={{
                width: 200,
                height: 40,
                borderColor: "gray",
                borderWidth: 1
              }}
              onChangeText={text => this.setState({ roomID: text })}
              value={this.state.roomID}
            />
            <TouchableHighlight onPress={this._press}>
              <Text>Enter room</Text>
            </TouchableHighlight>
          </View>
        ) : null}
        <RTCView streamURL={this.state.selfViewSrc} style={styles.selfView} />

        <RTCView streamURL={this.state.remoteTestSrc} style={styles.selfView} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  selfView: {
    position: "absolute",
    top:
      Platform.OS === "ios" ? 0 : -ExtraDimensions.get("STATUS_BAR_HEIGHT") + 1,
    width: Dimensions.get("window").width / 3,
    height: Dimensions.get("window").height / 3,
    borderWidth: 2
    // transform: [{ scaleX: (Platform.OS === 'ios') ? -1 : 1 }],
  },
  remoteView: {
    width: 200,
    height: 150
  },
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  listViewContainer: {
    height: 150
  }
});

// AppRegistry.registerComponent('RCTWebRTCDemo', () => RCTWebRTCDemo);
