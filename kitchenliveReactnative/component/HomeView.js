import React, { Component, } from 'react'
import { View, Text, StyleSheet, ListView, TouchableHighlight, Button} from 'react-native'
import MyScene from './MyScene';


class HomeView extends Component {

  static propTypes = {}

  static defaultProps = {}
  
  constructor(props) {
    super(props)
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows([
        'John', 'Joel', 'James', 'Jimmy', 'Jackson', 'Jillian', 'Julie', 'Devin'
      ])
    };    
  }
  
  _onPressButton() {
    console.log("You tapped the button!");
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) => 
        <TouchableHighlight onPress={this._onPressButton}> 
          <Text>{rowData}</Text>
        </TouchableHighlight>}
        />
        {/*<Button onPress={this.props.onPress} title='(Main) Go To Login Scene' />*/}
      </View>
    );
  }
}
// <Button onPress={this.props.onPress} title='(Main) Go To Login Scene' />

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default HomeView
