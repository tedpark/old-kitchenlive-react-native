import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  ScrollView,
  Modal,
  Dimensions,
  Button
} from "react-native";

// import { StackNavigator } from 'react-navigation';
import Icon from "react-native-vector-icons/FontAwesome";
import { COLOR } from "react-native-material-ui";
// import { Tabs, Tab } from 'react-native-elements';

import HomeView from "./HomeView";
// import BroadcastView from './BroadcastView';
import StudentsView from "./StudentsView";
// import LoginView from './LoginView';
import MainListView from "./MainListView";
import MainListDetailView from "./MainListDetailView";
import ChefView from "./ChefView";

// you can set your style right here, it'll be propagated to application
const uiTheme = {
  palette: {
    primaryColor: COLOR.green500
  },
  toolbar: {
    container: {
      height: 50
    }
  }
};

let testBool = false;
var initialLoginBool = true;

class MainNavView extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);

    this.state = {
      selectedTab: "profile",
      modalVisible: false,
      modalVisibleLogin: false,
      selectedListItem: ""
    };

    this.changeTab = this.changeTab.bind(this);
  }

  setModalVisible(visible) {
    this.setState({
      modalVisible: visible
      // modalVisibleLogin: visible,
    });
  }

  changeTab(selectedTab) {
    this.setState({
      selectedTab
    });
  }

  _qwer = () => {
    console.log("qwer11234");
    this.setModalVisible(true);
  };

  loginCheck = () => {
    console.log("loginCheck");
  };

  _handleAction(action, i) {
    console.log("if (key === MainListView");
    console.log(i);
    const newState = NavReducer(this.state.navState, action);
    if (newState === this.state.navState) {
      return false;
    }
    this.setState({
      selectedListItem: i,
      navState: newState
    });
    return true;
  }
  _handleBackAction = () => {
    console.log("_handleBackAction");
    return this._handleAction({ type: "pop" });
  };
  _renderRoute(key) {
    // <MainListView/>
    if (key === "MainListView") {
      return (
        <MainListView
          onPress={this._handleAction.bind(this, {
            type: "push",
            key: "MainListDetailView"
          })}
        />
      );
    }
    if (key === "MainListDetailView") {
      return (
        <MainListDetailView
          selectedListItem={this.state.selectedListItem}
          // onPress={this._handleAction.bind(this, { type: 'push', key: 'About' })}
        />
      );
    }
    if (key === "About") {
      return (
        <HomeView
          goBack={this._handleBackAction.bind(this)}
          onPress={this._handleAction.bind(this, {
            type: "push",
            key: "Contact"
          })}
        />
      );
    }
    if (key === "Contact") {
      return <Contact goBack={this._handleBackAction.bind(this)} />;
    }
  }

  /*
  _renderScene(props) {
      const ComponentToRender = this._renderRoute(props.scene.route.key)
      return (
        <ScrollView style={styles.scrollView}>
          {ComponentToRender}
        </ScrollView>
      );
  }
  */

  // <ScrollView>
  _renderScene(props) {
    const ComponentToRender = this._renderRoute(props.scene.route.key);
    return (
      <View style={styles.scrollView}>
        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            alert("Modal has been closed.");
          }}
        >
          <View>
            <TouchableHighlight
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
                console.log("ios close");
              }}
            >
              <View
                style={{
                  width: 50,
                  height: 50,
                  backgroundColor: "steelblue",
                  position: "absolute",
                  top: Dimensions.get("window").height - 50
                }}
              />
            </TouchableHighlight>

            <ChefView
              closeBtn={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}
            />
          </View>
        </Modal>

        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.modalVisibleLogin}
          onRequestClose={() => {
            alert("Modal has been closed.");
          }}
        >
          <View>
            <StudentsView
              closeBtn={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}
            />
          </View>
        </Modal>

        {/* */}
        {/* 여기에 쿠킹 클래스 리스트 뷰 만들면 됨 */}
        {ComponentToRender}
      </View>
    );
  }

  _renderHeader = sceneProps => {
    return (
      <Header
        qwer={this._qwer}
        back={this._handleBackAction}
        navigate={this._navigate}
        {...sceneProps}
      />
    );
  };

  static navigationOptions = {
    title: "Welcome"
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.scrollView}>
        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            alert("Modal has been closed.");
          }}
        >
          <View>
            <TouchableHighlight
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
                console.log("ios close");
              }}
            >
              <View
                style={{
                  width: 50,
                  height: 50,
                  backgroundColor: "steelblue",
                  position: "absolute",
                  top: Dimensions.get("window").height - 50
                }}
              />
            </TouchableHighlight>

            <ChefView
              closeBtn={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}
            />
          </View>
        </Modal>

        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.modalVisibleLogin}
          onRequestClose={() => {
            alert("Modal has been closed.");
          }}
        >
          <View>
            <StudentsView
              closeBtn={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}
            />
          </View>
        </Modal>

        {/* */}
        {/* 여기에 쿠킹 클래스 리스트 뷰 만들면 됨 */}
        <Button
          onPress={() => navigate("MainListDetailView", { user: "Lucy" })}
          title="Chat with Lucy"
        />

        <MainListView
          onPress={() => navigate("MainListDetailView", { user: "Lucy" })}
        />
      </View>
    );
  }
}

class Header extends Component {
  _back = () => {
    // console.log(this.props)
    // this.props.qwer()
    this.props.back();
    // return this._handleAction({ type: 'pop' });
    // this.props.navigate('pop');
    // return this._handleAction({ type: 'pop' });
  };
  _renderTitleComponent = props => {
    return (
      <NavigationHeader.Title>{props.scene.route.key}</NavigationHeader.Title>
    );
  };
  _goLink = () => {
    console.log("goLink");
    // console.log(this.props)
    this.props.qwer();
  };

  render() {
    return (
      <NavigationHeader
        {...this.props}
        renderTitleComponent={this._renderTitleComponent}
        renderRightComponent={props => (
          <Icon
            name="podcast"
            size={24}
            color="red"
            onPress={this._goLink}
            style={{ height: 24, width: 24, margin: 10 }}
          />
        )}
        onNavigateBack={this._back}
      />
    );
  }
}

const Button1 = ({ title, onPress }) => (
  <TouchableHighlight
    underlayColor="#EFEFEF"
    onPress={onPress}
    style={styles.button}
  >
    <Text>{title}</Text>
  </TouchableHighlight>
);

const Home = ({ onPress }) => (
  <View style={styles.container}>
    <Text style={styles.title}>Hello From Home</Text>
    <Button onPress={onPress} title="Go To Next Scene" />
  </View>
);

const About = ({ onPress, goBack }) => (
  <View style={styles.container}>
    <Text style={styles.title}>Hello From About</Text>
    <Button onPress={onPress} title="Go To Next Scene" />
    <Button onPress={goBack} title="Go Back" />
  </View>
);

const Contact = ({ goBack }) => (
  <View style={styles.container}>
    <Text style={styles.title}>Hello From Contact</Text>
    <Button title="Go Back" onPress={goBack} />
  </View>
);

const styles = StyleSheet.create({
  scrollView: {
    // backgroundColor: '#F5FCFF',
    flex: 1
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
    // backgroundColor: '#F5FCFF',
  },
  title: {
    fontSize: 40,
    marginTop: 200,
    textAlign: "center"
  },
  button: {
    height: 70,
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: "#EDEDED"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});

export default MainNavView;
