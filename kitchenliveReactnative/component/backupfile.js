<Text style={styles.textStyle}>신청 하기</Text>
    <View style={{ height: 40 }} />
                  {
    this.state.isYours ? null : this.state.isCancel ? (
        <View>
            {params.json.paidStudents.includes(this.state.userId) ? (
                <Button
                    raised
                    backgroundColor="green"
                    disabled={
                        new Date(params.json.broadcastDate) <
                            new Date(new Date().getTime() - 3 * 1000 * 60 * 60)
                            ? true
                            : false
                    }
                    onPress={this._onPress}
                    textStyle={{ fontSize: 14, fontWeight: "bold" }}
                    title="방송 입장하기!!"
                    borderRadius={2}
                />
            ) : (
                    <View>
                        <Text style={styles.labelDetailStyle}>
                            1. 프로필의 전화번호와 주소가 제대로 입력이 되었는지
                            확인 주세요. {"\n"}
                            신청하고 확인 문자가 없으면 "성함"과 "주소"
                            "요리날짜"가 포함된 "캡쳐 화면"을 {"\n"}
                            문자로 전송해주세요. (수강 가능여부를 확인하고 안내
                            문자를 보내드립니다.)
                          </Text>
                        <Text style={styles.labelDetailStyle}>
                            2. 온라인 수강신청이 완료 되면 "방송참여" 버튼이
                            생기고{"\n"}
                            회원님의 주소로 키친박스가 배송 시작 됩니다.
                          </Text>
                        <Text style={styles.labelDetailStyle}>
                            3. 요리 시간에 맞춰 미리 준비된 조리기구와{"\n"}
                            배송키트를 가지고 실시간 요리방송을 시작합니다.{
                                "\n"
                            }
                        </Text>
                        {/* {Platform.OS === "ios" ? null :
                            <View>
                              <Text style={{
                                textAlign: "center",
                                fontSize: 14,
                                fontWeight: "bold",
                                color: "tomato",
                                lineHeight: 20,
                              }}>키친라이브 계좌번호(우리은행 함세정)</Text> <Text selectable style={styles.labelDetailStyle}>1002434294796</Text>
                              <View style={{ height: 20 }}></View>
                            </View>
                          } */}
                        <Text
                            style={{
                                textAlign: "center",
                                fontSize: 14,
                                fontWeight: "bold",
                                color: "tomato",
                                lineHeight: 20
                            }}
                        >
                            {" "}
                            키친라이브 번호(문자로 보내주세요)
                          </Text>
                        <Text selectable style={styles.labelDetailStyle}>
                            01052727394
                          </Text>
                    </View>
                )}

            <View style={{ height: 40 }} />

            <Button
                raised
                backgroundColor="blue"
                onPress={this.attendCancelRequestPress}
                textStyle={{ fontSize: 14, fontWeight: "bold" }}
                title="취소 신청하기!!"
                borderRadius={2}
            />
            <View style={{ height: 40 }} />
        </View>
    ) : (
            <View>
                {!params.json.freeToAttend &&
                    params.json.students.length > 6 ? null : (
                        <Button
                            raised
                            backgroundColor="green"
                            onPress={this.attendRequestPress}
                            textStyle={{ fontSize: 14, fontWeight: "bold" }}
                            title="신청하기!!"
                            borderRadius={2}
                        />
                    )}
                <View style={{ height: 40 }} />
            </View>
        )
}



//////////////////////////////////////////////////////////////////////////////

<Text style={styles.textStyle}>참가 확정 인원</Text>
{/* <Text style={styles.labelStyle}>hoonistyle@naver.com</Text> */ }
{/* <Text style={styles.labelStyle}>soyeonjune@gmail.com</Text> */ }
{
    params.json.paidStudents.map((student, key) => {
        return (
            <Text key={key} style={styles.labelStyle}>
                {console.log(
                    this.state.meJson.filter(
                        value => value.userId === student
                    )[0]
                )}
                {this.state.meJson.filter(
                    value => value.userId === student
                )[0] === undefined
                    ? null
                    : this.state.meJson.filter(
                        value => value.userId === student
                    )[0].name}
            </Text>
        );
    })
}