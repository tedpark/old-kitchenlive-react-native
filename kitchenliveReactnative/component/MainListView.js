import React, { Component } from "react";
import { Card, COLOR } from "react-native-material-ui";
import {
  View,
  Text,
  ListView,
  StyleSheet,
  NativeModules,
  Image,
  Dimensions,
  Navigator,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Button,
  AsyncStorage,
  FlatList
} from "react-native";
import Entypo from "react-native-vector-icons/Entypo";
import Icon from "react-native-vector-icons/FontAwesome";
import AddBroadcast from "./AddBroadcast";
import serverUrl from "../config";
import Modal from "react-native-modal";
import { BackHandler } from "react-native";
const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;
import axios from "axios";

const uiTheme = {
  palette: {
    primaryColor: COLOR.green500
  },
  toolbar: {
    container: {
      height: 50
    }
  }
};

const rowHasChanged = (r1, r2) => r1 !== r2;

const testArray = ["a", "b", "c", "d"];
// const devServer = 'http://192.168.0.102:9000';
// const prodServer = 'https://kitchenlive.tv';
// const coServer = 'https://kitchenlive.co';
// var serverAddress = prodServer;

class MainListView extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    AsyncStorage.getItem("accessToken").then(value => {
      //  this.setState({'data': value});
      console.log(value);
    });
    this.state = {
      refreshing: false,
      testArray: [],
      cookJsonArray: [],
      modalChefViewVisible: false,
      showAddButton: true
    };
  }

  setModalVisible = visible => {
    this.setState({
      modalChefViewVisible: visible
      // modalVisibleLogin: visible,
    });
  };
  // this.props.navigation.navigate('MainListView', {name: 'Lucy'})
  // onPress={() => {state.params.setModalVisible(state.params.modalChefViewVisible);}}
  // onPress={() => navigate('MainListDetailView', { json: i })}
  static navigationOptions = ({ navigation, screenProps }) => ({
    title: "Kitchenlive.tv",
    headerTintColor: "tomato",
    headerBackTitle: null,
    gesturesEnabled: false,
    headerLeft: null,
    // headerLeft: (
    //   <Icon
    //     name="repeat"
    //     onPress={() => navigation.state.params.fetchData()}
    //     size={28}
    //     color="red"
    //     style={{ height: 34, width: 34, margin: 4 }}
    //   />
    // ),
    // headerLeft: <Icon name="navicon" onPress={() => navigation.navigate('SettingView', { json: "state" })} size={28} color="red" style={{height: 34, width: 34, margin:4}}/>,
    headerRight:
      serverUrl !== "https://kitchenlive.tv" ? (
        <Icon
          name="plus-circle"
          onPress={() => {
            navigation.state.params.setModalVisible(
              navigation.state.params.modalChefViewVisible
            );
          }}
          size={28}
          color="tomato"
          style={{ height: 34, width: 34, margin: 4 }}
        />
      ) : null
  });
  // header: ({ state, setParams, navigate }) => ({
  //   left:  <Icon name="navicon" onPress={() => navigate('SettingView', { json: state })} size={28} color="red" style={{height: 34, width: 34, margin:4}}/>,
  //   right:  <Icon name="podcast" onPress={() => {state.params.setModalVisible(state.params.modalChefViewVisible);}} size={28} color="red" style={{height: 34, width: 34, margin:4}}/>
  // })

  _onPressButton(i) {
    console.log("1_onPressButton");
    console.log(i);
    this.props.onPress(i.roomName);
  }

  // fetchData = () => {
  //   AsyncStorage.getItem("accessToken").then(value => {
  //     axios
  //       .get(serverUrl + "/api/broadcast-lists", { Authorization: "Bearer " + value })
  //       .then(response => {
  //         console.log(response.data);
  //         this.setState({
  //           cookJsonArray: response.data,
  //           refreshing: false
  //         });
  //         console.log(response.data);
  //       })
  //       .catch(function (error) {
  //         console.log(error);
  //       });
  //   });
  // }

  fetchData = () => {
    AsyncStorage.getItem("accessToken").then(value => {
      fetch(serverUrl + "/api/broadcast-lists", {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Bearer " + value
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log(this.props.navigation.state.params.userId);
          // console.log(
          //   responseJson.filter(jsonData => {
          //     // console.log(jsonData.paidStudents);
          //     return (
          //       jsonData.paidStudents.includes(
          //         this.props.navigation.state.params.userId
          //       ) || jsonData.freeToAttend === true
          //     );
          //   })
          // );
          this.setState({
            cookJsonArray: responseJson.filter(jsonData => {
              return (
                jsonData.paidStudents.includes(
                  this.props.navigation.state.params.userId
                ) || jsonData.freeToAttend === true
              );
            }),
            refreshing: false
          });
          console.log(responseJson);
        })
        .catch(error => {
          console.log("Api call error");
          console.log(error);
        })
        .done();
      console.log(value);
    });
  };

  componentDidMount() {
    console.log("MainListView");
    console.log(this.props);
    this.props.navigation.setParams({
      setModalVisible: this.setModalVisible,
      modalChefViewVisible: !this.state.modalChefViewVisible,
      fetchData: this.fetchData
    });
    this.fetchData();
    console.log(this.props.navigation.state.params.userId);
  }

  handleRefresh = () => {
    this.fetchData();
  };

  render() {
    const { navigate } = this.props.navigation;
    const IMAGES = {
      image0: require("../image/0.jpg"), // statically analyzed
      image1: require("../image/1.jpg"), // statically analyzed
      image2: require("../image/2.jpg"), // statically analyzed
      image3: require("../image/3.jpg"), // statically analyzed
      image4: require("../image/4.jpg"), // statically analyzed
      image5: require("../image/5.jpg"), // statically analyzed
      image6: require("../image/6.jpg"), // statically analyzed
      image7: require("../image/7.jpg") // statically analyzed
    };
    let index = 0;
    // <Text style ={ styles.item} >{ i.roomName } </Text>

    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.cookJsonArray}
          keyExtractor={(item, index) => index.toString()}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
          removeClippedSubviews={false}
          renderItem={({ item }) => (
            <TouchableWithoutFeedback
              onPress={() =>
                navigate("MainListDetailView", {
                  hideTabBar: true,
                  json: item,
                  sss: navigate,
                  index: index,
                  refresh: this.fetchData
                })
              }
            >
              <View>
                <Card>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View
                      style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center",
                        // height: 330,
                        height: aspectRatio > 1.6 ? 230 + 120 : 230 + 120 + 350,
                        borderWidth: 0,
                        borderColor: "lightgray",
                        marginLeft: 0
                      }}
                    >
                      {this.state.cookJsonArray.length > 0 ? (
                        <Image
                          style={{
                            width: Dimensions.get("window").width - 30,
                            // height: 200,
                            height: aspectRatio > 1.6 ? 230 : 230 + 350,
                            borderRadius: 8,
                            margin: 4
                          }}
                          source={{ uri: item.images[0] }}
                        />
                      ) : null}

                      <Text style={styles.item}>{item.roomName}</Text>

                      <Text style={styles.item}>
                        {new Date(item.broadcastDate).getMonth() +
                          1 +
                          "월" +
                          " " +
                          new Date(item.broadcastDate).getDate() +
                          "일" +
                          " " +
                          new Date(item.broadcastDate).getHours() +
                          "시" +
                          " " +
                          new Date(item.broadcastDate).getMinutes() +
                          "분"}
                      </Text>
                      <Text style={styles.item}>
                        {"참가신청 " + item.paidStudents.length + "명"}
                      </Text>
                    </View>
                  </View>
                </Card>
              </View>
            </TouchableWithoutFeedback>
          )}
        />

        <Modal
          animationIn={"slideInUp"}
          animationOut={"slideOutDown"}
          isVisible={this.state.modalChefViewVisible}
          style={{ margin: 0 }}
          // onRequestClose={onBackButtonPress}
        >
          <View style={{ flex: 1 }}>
            <AddBroadcast
              close={() => {
                this.setModalVisible(!this.state.modalChefViewVisible);
              }}
              refresh={() => {
                this.fetchData();
              }}
            />
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    paddingTop: 0
  },
  item: {
    fontSize: 14,
    fontWeight: "bold",
    margin: 5,
    padding: 5,
    color: "#424242"
    // backgroundColor: "ghostwhite",
    // textAlign: "center"
  }
});

export default MainListView;

// <Modal animationType={"slide"} transparent={false} visible={this.state.modalChefViewVisible} onRequestClose={() => {alert("Modal has been closed.");}}>
//   <View>
//   <TouchableHighlight onPress={() => {
//     this.setModalVisible(!this.state.modalChefViewVisible);
//     console.log('ios close');
//   }}>
//   <View style={{width: 50, height: 50, backgroundColor: 'steelblue', position: 'absolute', top: Dimensions.get("window").height - 50}} />
//   </TouchableHighlight>
//   <ChefView closeBtn={() => {this.setModalVisible(!this.state.modalChefViewVisible);}}/>
//   </View>
// </Modal>

// 1. broadcastDate:"1909-01-01T00:00:00.000Z"
// 2. category:"category-mexico"
// 3. roomDescription:"roomDescription-garak"
// 4. roomId:"zaabc"
// 5. roomName:"roomName-test"
// 6. students:Array(1)
// 7. userId:"userId-Ted"
// 8. __v:0
// 9. _id:"58ee762ae2eab32b99937080"
