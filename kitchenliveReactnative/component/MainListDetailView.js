import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  NativeModules,
  ScrollView,
  // Modal,
  TouchableHighlight,
  Dimensions,
  FlatList,
  Image,
  AsyncStorage,
  Platform,
  Alert
} from "react-native";
// const { InAppUtils } = NativeModules
import Modal from "react-native-modal";
import Icon from "react-native-vector-icons/FontAwesome";
import { Card, ThemeProvider, COLOR, Checkbox } from "react-native-material-ui";
import StudentsViewJanus from "./StudentsViewJanus";
import { ListItem, Button, Tile, Divider } from "react-native-elements";
// import InApp from "./InApp"
import axios from "axios";
const devServer = "http://192.168.0.102:9000";
const prodServer = "https://kitchenlive.tv";
const coServer = "https://kitchenlive.co";
var serverAddress = prodServer;
import serverUrl from "../config";
const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

const uiTheme = {
  palette: {
    primaryColor: COLOR.green500
  },
  toolbar: {
    container: {
      height: 50
    }
  }
};

class MainListDetailView extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      selectedTab: "profile",
      modalVisible: false,
      modalIAPVisible: false,
      refreshing: false,
      close: null,
      attendButtonState: false,
      dateTime: new Date(),
      attendant: [],
      isYours: false,
      isAttendant: false,
      isCancel: false,
      meJson: [],
      userIdName: "",
      userId: ``
    };
    console.log(props.selectedListItem);
    console.log(props.navigation);
  }

  deleteBroadcast() {
    console.log("deleteBroadcast");
    this.props.navigation.goBack();
  }

  setModalVisible(visible) {
    this.setState({
      modalVisible: visible
    });
  }

  attendRequestPress = () => {
    AsyncStorage.getItem("meJson").then(me => {
      const { params } = this.props.navigation.state;
      console.log(params);
      console.log(JSON.parse(me)._id);
      AsyncStorage.getItem("accessToken").then(token => {
        console.log("updateBroadcast");
        // params.json.category = "변경";
        params.json.students.push(JSON.parse(me)._id);
        axios({
          method: "put",
          url: serverAddress + "/api/broadcast-lists/" + params.json._id,
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token
          },
          data: params.json
        }).then(response => {
          console.log(response);
          if (this.state.isYours) {
            console.log(this.state.attendant);
            this.setState({ isCancel: true });
            this.setState({ isAttendant: false });
            console.log("this.setState({ isCancel: true });");
          } else {
            if (params.json.students.includes(JSON.parse(me)._id)) {
              this.setState({ isCancel: true });
              this.setState({ isAttendant: false });
              console.log("isCancel: true");
              this.setState({
                meJson: [...this.state.meJson, response.data]
              });
            } else {
              this.setState({ isCancel: false });
              this.setState({ isAttendant: true });
              console.log("isAttendant: true");
              // this.setState({
              //   meJson: [...this.state.meJson, response.data],
              // });
            }
          }
        });
      });
    });
  };

  buySubscriptionOneMonth = () => {};

  attendCancelRequestPress = () => {
    AsyncStorage.getItem("meJson").then(me => {
      const { params } = this.props.navigation.state;
      console.log(params);
      AsyncStorage.getItem("accessToken").then(token => {
        console.log("updateBroadcast");
        // params.json.category = "변경";
        params.json.students.splice(
          params.json.students.indexOf(params.json._id),
          1
        );
        axios({
          method: "put",
          url: serverAddress + "/api/broadcast-lists/" + params.json._id,
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token
          },
          data: params.json
        }).then(response => {
          console.log(response);
          this.setState({ isCancel: false });
          this.setState({ isAttendant: true });
        });
      });
    });
  };

  _onPress = () => {
    console.log("_onPress");
    this.setModalVisible(true);
  };

  // _onIAPPress = () => {
  //   InAppUtils.restorePurchases((error, response) => {
  //     if (error) {
  //       Alert.alert('itunes Error', 'Could not connect to itunes store.');
  //     } else {
  //       // Alert.alert('Restore Successful', 'Successfully restores all your purchases.');

  //       if (response.length === 0) {
  //         // Alert.alert('No Purchases', "We didn't find any purchases to restore.");
  //         console.log("_onIAPPress");
  //         this.setIAPModalVisible(true);
  //         return;
  //       }

  //       response.forEach((purchase) => {
  //         if (purchase.productIdentifier === 'Monthlysubscription') {
  //           // Handle purchased product.
  //           this.buySubscriptionOneMonth()
  //         }
  //       });
  //     }
  //   });
  // };

  _onPressDelete = () => {
    fetch(serverAddress + "/api/broadcast-lists")
      .then(response => {
        this.props.navigation.goBack();
        response.json();
      })
      .then(responseJson => {
        console.log(responseJson);
      })
      .catch(error => {
        console.log("Api call error");
        console.log(error);
      });
  };

  fetchDeleteBroadcast = params => {
    // const { params } = this.props.navigation.state;
    console.log(params);
    AsyncStorage.getItem("meJson").then(me => {
      if (JSON.parse(me)._id === params.json.userId) {
        console.log("true");
        AsyncStorage.getItem("accessToken").then(token => {
          console.log("DeleteBroadcast");

          fetch(serverUrl + "/api/broadcast-lists/" + params.json._id, {
            method: "DELETE",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + token
            }
          })
            .then(response => response.text())
            .then(responseData => {
              console.log("response object:", responseData);
              params.refresh();
              this.props.navigation.goBack();
            })
            .catch(error => {
              console.error(error);
            });
        });
      } else {
        console.log("false");
      }
    });
  };

  componentDidMount() {
    const { params } = this.props.navigation.state;
    console.log(this.props.navigation.state.params);

    for (let values of params.json.students) {
      console.log(values);
      this.getProfile(values);
    }

    this.props.navigation.setParams({
      goBack: true
    });

    AsyncStorage.getItem("meJson").then(me => {
      this.setState({ userIdName: JSON.parse(me).name });
      this.setState({ userId: JSON.parse(me)._id });

      console.log(JSON.parse(me)._id);

      if (params.json.students.includes(JSON.parse(me)._id)) {
        this.setState({ attendButtonState: true });
        console.log("true");
      } else {
        this.setState({ attendButtonState: false });
        console.log("false");
      }

      if (this.state.isYours) {
      } else {
        if (params.json.students.includes(JSON.parse(me)._id)) {
          this.setState({ isCancel: true });
          console.log("isCancel: true");
        } else {
          this.setState({ isCancel: false });
          this.setState({ isAttendant: true });
          console.log("isAttendant: true");
        }
      }

      if (JSON.parse(me)._id === params.json.userId) {
        console.log("true");
        this.setState({
          isYours: true
        });
      } else {
        this.setState({
          isYours: false
        });
      }
    });

    // InAppUtils.canMakePayments((canMakePayments) => {
    //   if (!canMakePayments) {
    //     Alert.alert('Not Allowed', 'This device is not allowed to make purchases. Please check restrictions on device');
    //   } else {

    //   }
    // })
  }

  handleRefresh = () => {
    this.setState({
      page: 1,
      seed: this.state.seed + 1,
      refreshing: true
    });
  };

  getProfile(userId) {
    console.log(userId);
    let config = {
      Authorization: "Bearer " + this.props.navigation.state.params.token
    };
    axios
      .get(serverUrl + "/api/profiles/" + userId, config)
      .then(response => {
        this.setState({ meJson: [] }, () => {
          this.setState({
            meJson: [...this.state.meJson, response.data],
            refreshing: false
          });
        });
        console.log(response);
        console.log(this.state.meJson);
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  static navigationOptions = {
    // title: "Kitchenlive.tv",
    headerTintColor: "tomato"
  };

  render() {
    const { params } = this.props.navigation.state;
    const IMAGES = {
      image0: require("../image/0.jpg"), // statically analyzed
      image1: require("../image/1.jpg"), // statically analyzed
      image2: require("../image/2.jpg"), // statically analyzed
      image3: require("../image/3.jpg"), // statically analyzed
      image4: require("../image/4.jpg"), // statically analyzed
      image5: require("../image/5.jpg"), // statically analyzed
      image6: require("../image/6.jpg"), // statically analyzed
      image7: require("../image/7.jpg") // statically analyzed
    };
    let index = 0;
    console.log(params);
    return (
      <View>
        <FlatList
          data={[{ key: "item1", key: "item2" }]} // refreshing={this.state.refreshing}
          // onRefresh={this.handleRefresh}
          renderItem={({ item }) => (
            <View>
              <Card>
                <Text style={styles.textStyle}>요리 이름</Text>
                <Text style={styles.labelStyle}>{params.json.roomName}</Text>
                {params.json.images.map((image, key) => {
                  return (
                    <View
                      key={key}
                      style={{
                        flex: 1,
                        flexDirection: "column",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Image
                        style={{
                          width: Dimensions.get("window").width - 30,
                          // height: 200,
                          height: aspectRatio > 1.6 ? 230 : 230 + 350,
                          borderRadius: 8,
                          margin: 4
                        }}
                        source={{ uri: image }}
                      />
                    </View>
                  );
                })}
                {/* <Text style={styles.textStyle}>
                {new Date(params.json.broadcastDate).getFullYear()}
              </Text> */}
                <Divider
                  style={{ backgroundColor: "#C8C8CD", marginTop: 10 }}
                />
                <Text style={styles.textStyle}>요리 날짜</Text>
                <Text style={styles.labelStyle}>
                  {new Date(params.json.broadcastDate).getFullYear() +
                    "년" +
                    " " +
                    (new Date(params.json.broadcastDate).getMonth() + 1) +
                    "월" +
                    " " +
                    new Date(params.json.broadcastDate).getDate() +
                    "일" +
                    " " +
                    new Date(params.json.broadcastDate).getHours() +
                    "시" +
                    " " +
                    new Date(params.json.broadcastDate).getMinutes() +
                    "분"}
                </Text>
                <Divider
                  style={{ backgroundColor: "#C8C8CD", marginTop: 10 }}
                />
                <Text style={styles.textStyle}>요리 설명</Text>
                <Text style={styles.labelDetailStyle}>
                  {params.json.roomDescription}
                </Text>
                <Divider
                  style={{ backgroundColor: "#C8C8CD", marginTop: 10 }}
                />
                <Text style={styles.textStyle}>
                  요리 재료들 (쿠킹박스로 배송 됩니다)
                </Text>
                {params.json.ingredients.map((ingredient, key) => {
                  return (
                    <Text key={key} style={styles.labelStyle}>
                      {ingredient}
                    </Text>
                  );
                })}
                <Divider
                  style={{ backgroundColor: "#C8C8CD", marginTop: 10 }}
                />
                <Text style={styles.textStyle}>
                  키친 기구들 (준비해야 해요)
                </Text>
                {params.json.kitchenTools.map((kitchenTool, key) => {
                  return (
                    <Text key={key} style={styles.labelStyle}>
                      {kitchenTool}
                    </Text>
                  );
                })}
                <Divider
                  style={{ backgroundColor: "#C8C8CD", marginTop: 10 }}
                />
                {this.state.isYours ||
                this.props.navigation.state.params.json.chef ===
                  this.state.userId ? (
                  <View>
                    <View style={{ height: 40 }} />
                    <Button
                      raised
                      backgroundColor="green"
                      // disabled={
                      //   new Date(params.json.broadcastDate) <
                      //     new Date(new Date().getTime() - 3 * 1000 * 60 * 60)
                      //     ? true
                      //     : false
                      // }
                      onPress={this._onPress}
                      textStyle={{ fontSize: 14, fontWeight: "bold" }}
                      title="쉐프님 입장하기!!"
                      borderRadius={2}
                    />

                    <View style={{ height: 40 }} />

                    <Button
                      raised
                      backgroundColor="red"
                      onPress={() => {
                        this.fetchDeleteBroadcast(params);
                      }}
                      textStyle={{ fontSize: 14, fontWeight: "bold" }}
                      title="삭제하기!!"
                      borderRadius={2}
                    />
                  </View>
                ) : params.json.freeToAttend ? (
                  <View>
                    <View style={{ height: 40 }} />
                    <Button
                      raised
                      backgroundColor="green"
                      // disabled={
                      //   new Date(params.json.broadcastDate) <
                      //     new Date(new Date().getTime() - 3 * 1000 * 60 * 60)
                      //     ? true
                      //     : false
                      // }
                      onPress={this._onPress}
                      textStyle={{ fontSize: 14, fontWeight: "bold" }}
                      title="오픈 쿡방 입장하기!!"
                      borderRadius={2}
                    />
                  </View>
                ) : params.json.paidStudents.includes(this.state.userId) ? (
                  <View>
                    <View style={{ height: 40 }} />
                    <Button
                      raised
                      backgroundColor="green"
                      disabled={
                        new Date(params.json.broadcastDate) <
                        new Date(new Date().getTime() - 3 * 1000 * 60 * 60)
                          ? true
                          : false
                      }
                      onPress={this._onPress}
                      textStyle={{ fontSize: 14, fontWeight: "bold" }}
                      title="쿡방 입장하기!!"
                      borderRadius={2}
                    />
                  </View>
                ) : null}

                <View style={{ height: 40 }} />
              </Card>
            </View>
          )}
        />
        <Modal
          animationIn={"slideInUp"}
          animationOut={"slideOutDown"}
          isVisible={this.state.modalVisible}
          style={{ margin: 0, backgroundColor: "white" }}
          // onRequestClose={onBackButtonPress}
        >
          <View style={{ flex: 1 }}>
            <StudentsViewJanus
              closeBtn={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}
              i={params}
              roomName={params}
              userIdName={this.state.userIdName}
              params={this.props.navigation.state}
              meJson={this.state.meJson}
              userId={this.state.userId}
            />
          </View>
        </Modal>
      </View>
    );
  }
}

// <FlatList
//       data={[{key: 'a'}, {key: 'b'}]}
//       renderItem={({item}) => <Text>{item.key}</Text>}
//     />
// <Text style={styles.textStyle}>{params.json.roomName}</Text>
// <Text style={styles.textStyle}>{params.json.broadcastDate}</Text>
// <Text style={styles.textStyle}>{params.json.category}</Text>
// <Text style={styles.textStyle}>{params.json.roomDescription}</Text>
// <Text style={styles.textStyle}>{params.json.students}</Text>
// <Text style={styles.textStyle}>{params.json.roomId}</Text>
// <Text style={styles.textStyle}>{params.json.userId}</Text>

const styles = StyleSheet.create({
  headingContainer: {
    marginTop: 60,
    justifyContent: "center",
    alignItems: "center",
    padding: 40,
    backgroundColor: COLOR.red100
  },
  heading: {
    color: "white",
    marginTop: 10,
    fontSize: 22
  },
  labelContainerStyle: {
    marginTop: 8
  },
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 20,
    paddingTop: 20
  },
  formLabel: {
    fontSize: 16,
    fontWeight: "bold"
  },
  textStyle: {
    margin: 4,
    marginLeft: 8 + 0,
    textAlign: "left",
    fontSize: 14,
    fontWeight: "bold",
    color: "tomato"
  },
  labelStyle: {
    margin: 12,
    marginLeft: 12 + 6,
    textAlign: "center",
    fontSize: 14,
    fontWeight: "bold",
    color: "#424242"
  },
  labelDetailStyle: {
    margin: 4,
    // marginLeft: 12 + 6,
    textAlign: "center",
    fontSize: 14,
    fontWeight: "bold",
    color: "#424242",
    lineHeight: 30
  }
});

// const styles = StyleSheet.create({
//   headingContainer: {
//     marginTop: 60,
//     justifyContent: "center",
//     alignItems: "center",
//     padding: 40,
//     backgroundColor: COLOR.red100
//   },
//   heading: {
//     color: "white",
//     marginTop: 10,
//     fontSize: 22
//   },
//   labelContainerStyle: {
//     marginTop: 8
//   },
//   container: {
//     flex: 1,
//     justifyContent: "center",
//     paddingHorizontal: 20,
//     paddingTop: 20
//   },
//   formLabel: {
//     fontSize: 16,
//     fontWeight: "bold"
//   },
//   textStyle: {
//     margin: 12,
//     marginLeft: 12 + 6,
//     textAlign: "left",
//     fontSize: 16,
//     fontWeight: "bold",
//     color: "darkslategray"
//   }
// });

// <View style={{width: 50, height: 50, backgroundColor: 'steelblue', position: 'absolute', top: Dimensions.get("window").height - 50}} />
export default MainListDetailView;

// {params.json.roomId}
// {params.json.userId}
