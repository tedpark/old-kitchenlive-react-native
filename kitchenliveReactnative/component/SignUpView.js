import React, { Component } from "react";
import { View, Text, StyleSheet, AsyncStorage } from "react-native";
import serverUrl from "component/config";
import {
  FormLabel,
  FormInput,
  FormValidationMessage,
  Button
} from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Card, COLOR } from "react-native-material-ui";
import axios from "axios";
import { TextField } from "react-native-material-textfield";

const uiTheme = {
  palette: {
    primaryColor: COLOR.green500
  },
  toolbar: {
    container: {
      height: 50
    }
  }
};

class SignUpView extends Component {
  static propTypes = {};

  static defaultProps = {};

  state = {};

  componentDidMount() {
    console.log(this.props.navigation.state.params.userId);
  }

  createProfile = () => {
    AsyncStorage.getItem("accessToken").then(value => {
      let config = {
        Authorization: "Bearer " + value
      };
      axios
        .post(
          serverUrl.SERVER_URL + "/api/profiles/",
          {
            name: this.state.name,
            kakaoId: this.state.kakaoId,
            address: this.state.address,
            cellPhone: this.state.cellPhone,
            userId: this.props.navigation.state.params.userId
          },
          config
        )
        .then(response => {
          console.log(response);
          this.props.navigation.navigate("Root", {
            name: "Lucy"
          });
        })
        .catch(function(error) {
          console.log(error);
        });
    });
  };

  render() {
    return (
      <KeyboardAwareScrollView style={{ marginTop: 0 }}>
        <View style={{ flex: 1 }}>
          <View style={styles.container}>
            <TextField
              label="이름을 입력해 주세요"
              multiline={true}
              autoFocus={true}
              value={this.state.name}
              onChangeText={name => this.setState({ name })}
            />

            <TextField
              label="카카오ID를 입력해 주세요"
              multiline={true}
              value={this.state.kakaoId}
              onChangeText={kakaoId => this.setState({ kakaoId })}
            />

            <TextField
              label="전화번호를 입력해 주세요"
              multiline={true}
              value={this.state.cellPhone}
              onChangeText={cellPhone => this.setState({ cellPhone })}
            />

            <TextField
              label="주소를 입력해 주세요"
              multiline={true}
              value={this.state.address}
              onChangeText={address => this.setState({ address })}
            />
          </View>

          <View style={{ marginBottom: 20 }} />

          <Button
            title="다음"
            raised
            buttonStyle={{ marginTop: 0 }}
            textStyle={{ fontSize: 14, fontWeight: "bold" }}
            backgroundColor="tomato"
            onPress={() => this.createProfile()}
            borderRadius={2}
          />
          <View style={{ marginBottom: 30 }} />
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  headingContainer: {
    marginTop: 60,
    justifyContent: "center",
    alignItems: "center",
    padding: 40
  },
  heading: {
    color: "white",
    marginTop: 10,
    fontSize: 22
  },
  labelContainerStyle: {
    marginTop: 8
  },
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 20,
    paddingTop: 20
  },
  formLabel: {
    fontSize: 16,
    fontWeight: "bold"
  },
  labelStyle: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#424242"
  },
  inputStyle: {
    height: 40,
    fontSize: 14,
    fontWeight: "bold",
    color: "darkslategrey"
  }
});

export default SignUpView;
