"use strict";

import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  TextInput,
  ListView,
  ScrollView,
  Platform,
  Dimensions,
  Button
} from "react-native";

// import devServer from '../config'
import InCallManager from "react-native-incall-manager";
import io from "socket.io-client/dist/socket.io";
import KeepAwake from "react-native-keep-awake";
const DEFAULT_URL = "https://kitchenlive.co";
import Icon from "react-native-vector-icons/FontAwesome";
import ExtraDimensions from "react-native-extra-dimensions-android";

// const socket = io(DEFAULT_URL, { transports: ["websocket"] });
const socket = io.connect(DEFAULT_URL);
// const socket = io.connect(DEFAULT_URL, {jsonp: false})

// if (devServer) {
//   socket = io.connect(DEFAULT_URL, {jsonp: false})
// } else {
//   socket = io.connect(DEFAULT_URL)
// }

import {
  RTCPeerConnection,
  RTCMediaStream,
  RTCIceCandidate,
  RTCSessionDescription,
  RTCView,
  MediaStreamTrack,
  getUserMedia
} from "react-native-webrtc";

// const configuration = { iceServers: [{ url: "stun:stun.l.google.com:19302" }] };
const configuration = {
  // turn:52.68.31.151:3478
  // turn:23.97.53.170:3478
  iceServers: [
    {
      urls: "turn:23.97.53.170:3478",
      username: "ted",
      credential: "park3764"
    }
  ]
};

const pcPeers = {};
let localStream;

function getLocalStream(isFront, callback) {
  MediaStreamTrack.getSources(sourceInfos => {
    console.log(sourceInfos);
    let videoSourceId;
    for (const i = 0; i < sourceInfos.length; i++) {
      const sourceInfo = sourceInfos[i];
      if (
        sourceInfo.kind == "video" &&
        sourceInfo.facing == (isFront ? "front" : "back")
      ) {
        videoSourceId = sourceInfo.id;
      }
    }
    getUserMedia(
      {
        audio: true,

        // video: false,

        video: {
          mandatory: {
            minWidth: 300 * 2, // Provide your own width, height and frame rate here
            minHeight: 168 * 2,
            minFrameRate: 15
          },
          facingMode: isFront ? "user" : "environment",
          optional: videoSourceId ? [{ sourceId: videoSourceId }] : []
        }
      },
      function (stream) {
        console.log("dddd", stream);
        callback(stream);
      },
      logError
    );
  });
}

function join(roomID) {
  socket.connect();
  socket.emit("join", roomID, function (socketIds) {
    console.log("join", socketIds);
    for (const i in socketIds) {
      const socketId = socketIds[i];
      createPC(socketId, true);
    }
  });
}

function createPC(socketId, isOffer) {
  const pc = new RTCPeerConnection(configuration);
  pcPeers[socketId] = pc;

  pc.onicecandidate = function (event) {
    console.log("onicecandidate", event.candidate);
    if (event.candidate) {
      socket.emit("exchange", { to: socketId, candidate: event.candidate });
    }
  };

  function createOffer() {
    pc.createOffer(function (desc) {
      console.log("createOffer", desc);
      pc.setLocalDescription(
        desc,
        function () {
          console.log("setLocalDescription", pc.localDescription);
          socket.emit("exchange", { to: socketId, sdp: pc.localDescription });
        },
        logError
      );
    }, logError);
  }

  pc.onnegotiationneeded = function () {
    console.log("onnegotiationneeded");
    if (isOffer) {
      createOffer();
    }
  };

  pc.oniceconnectionstatechange = function (event) {
    console.log("oniceconnectionstatechange", event.target.iceConnectionState);
    if (event.target.iceConnectionState === "completed") {
      setTimeout(() => {
        getStats();
      }, 1000);
    }
    if (event.target.iceConnectionState === "connected") {
      createDataChannel();
    }
  };
  pc.onsignalingstatechange = function (event) {
    console.log("onsignalingstatechange", event.target.signalingState);
  };

  pc.onaddstream = function (event) {
    console.log("onaddstream", event.stream);
    container.setState({ info: "One peer join!" });

    const remoteList = container.state.remoteList;
    remoteList[socketId] = event.stream.toURL();
    container.setState({ remoteList: remoteList });
  };
  pc.onremovestream = function (event) {
    console.log("onremovestream", event.stream);
  };

  pc.addStream(localStream);
  function createDataChannel() {
    if (pc.textDataChannel) {
      return;
    }
    const dataChannel = pc.createDataChannel("text");

    dataChannel.onerror = function (error) {
      console.log("dataChannel.onerror", error);
    };

    dataChannel.onmessage = function (event) {
      console.log("dataChannel.onmessage:", event.data);
      container.receiveTextData({ user: socketId, message: event.data });
    };

    dataChannel.onopen = function () {
      console.log("dataChannel.onopen");
      container.setState({ textRoomConnected: true });
    };

    dataChannel.onclose = function () {
      console.log("dataChannel.onclose");
    };

    pc.textDataChannel = dataChannel;
  }
  return pc;
}

function exchange(data) {
  const fromId = data.from;
  let pc;
  if (fromId in pcPeers) {
    pc = pcPeers[fromId];
  } else {
    pc = createPC(fromId, false);
  }

  if (data.sdp) {
    console.log("exchange sdp", data);
    pc.setRemoteDescription(
      new RTCSessionDescription(data.sdp),
      function () {
        if (pc.remoteDescription.type == "offer")
          pc.createAnswer(function (desc) {
            console.log("createAnswer", desc);
            pc.setLocalDescription(
              desc,
              function () {
                console.log("setLocalDescription", pc.localDescription);
                socket.emit("exchange", {
                  to: fromId,
                  sdp: pc.localDescription
                });
              },
              logError
            );
          }, logError);
      },
      logError
    );
  } else {
    console.log("exchange candidate", data);
    pc.addIceCandidate(new RTCIceCandidate(data.candidate));
  }
}

function leave(socketId) {
  console.log("leave", socketId);
  const pc = pcPeers[socketId];
  const viewIndex = pc.viewIndex;
  pc.close();
  delete pcPeers[socketId];

  const remoteList = container.state.remoteList;
  delete remoteList[socketId];
  container.setState({ remoteList: remoteList });
  container.setState({ info: "One peer leave!" });
}

socket.on("exchange", function (data) {
  exchange(data);
});
socket.on("leave", function (socketId) {
  leave(socketId);
});

// socket.on('connect', function(data) {
//   console.log('connect');
//   getLocalStream(true, function(stream) {
//     localStream = stream;
//     container.setState({selfViewSrc: stream.toURL()});
//     container.setState({status: 'ready', info: 'Please enter or create room ID'});
//   });
// });

function initStream() {
  InCallManager.start({ media: "video" });
  InCallManager.setMicrophoneMute(true);
  // InCallManager.track.enable = false
  // InCallManager.start({media: 'audio'});
  InCallManager.setForceSpeakerphoneOn.enable = true;
  InCallManager.turnScreenOn.enable = true;
  InCallManager.setKeepScreenOn.enable = true;
  getLocalStream(true, function (stream) {
    localStream = stream;
    container.setState({ selfViewSrc: stream.toURL() });
    container.setState({
      status: "ready",
      info: "Please enter or create room ID"
    });
  });
}

socket.on("connect", function (data) {
  console.log("connect");
});

function logError(error) {
  console.log("logError", error);
}

function mapHash(hash, func) {
  const array = [];
  for (const key in hash) {
    const obj = hash[key];
    array.push(func(obj, key));
  }
  return array;
}

function getStats() {
  const pc = pcPeers[Object.keys(pcPeers)[0]];
  if (
    pc.getRemoteStreams()[0] &&
    pc.getRemoteStreams()[0].getAudioTracks()[0]
  ) {
    const track = pc.getRemoteStreams()[0].getAudioTracks()[0];
    console.log("track", track);
    pc.getStats(
      track,
      function (report) {
        console.log("getStats report", report);
      },
      logError
    );
  }
}

let container;

class StudentsView extends Component {
  // const StudentsView = React.createClass({
  getInitialState() {
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => true });
    return {
      info: "Initializing",
      status: "init",
      roomID: "",
      isFront: true,
      selfViewSrc: null,
      remoteList: {},
      textRoomConnected: false,
      textRoomData: [],
      textRoomValue: ""
    };
  }

  componentWillMount() {
    socket.connect();
  }
  componentDidMount() {
    container = this;
    // socket.connect();
    initStream();
    //임의의 값 def 미리 입력
    console.log(this.props.i);
    console.log(this.props.i.json.roomId);
    join(this.props.i.json.roomId);
  }
  componentWillUnmount() {
    if (localStream) {
      for (const id in pcPeers) {
        const pc = pcPeers[id];
        pc && pc.removeStream(localStream);
        pc.close();
      }
      localStream.release();
    }
    socket.disconnect();
    // console.log('leave', this.props.i.json.roomId);
    // const pc = pcPeers[this.props.i.json.roomId];
    // pc.close();
  }

  _press(event) {
    this.refs.roomID.blur();
    this.setState({ status: "connect", info: "Connecting" });
    join(this.state.roomID);
  }
  _switchVideoType() {
    const isFront = !this.state.isFront;
    this.setState({ isFront });
    getLocalStream(isFront, function (stream) {
      if (localStream) {
        for (const id in pcPeers) {
          const pc = pcPeers[id];
          pc && pc.removeStream(localStream);
        }
        localStream.release();
      }
      localStream = stream;
      container.setState({ selfViewSrc: stream.toURL() });

      for (const id in pcPeers) {
        const pc = pcPeers[id];
        pc && pc.addStream(localStream);
      }
    });
  }
  receiveTextData(data) {
    const textRoomData = this.state.textRoomData.slice();
    textRoomData.push(data);
    this.setState({ textRoomData, textRoomValue: "" });
  }
  _textRoomPress() {
    if (!this.state.textRoomValue) {
      return;
    }
    const textRoomData = this.state.textRoomData.slice();
    textRoomData.push({ user: "Me", message: this.state.textRoomValue });
    for (const key in pcPeers) {
      const pc = pcPeers[key];
      pc.textDataChannel.send(this.state.textRoomValue);
    }
    this.setState({ textRoomData, textRoomValue: "" });
  }
  // _closeBtn = () => {
  //   this.localStream.release();
  //   this.props.closeBtn
  // }
  render() {
    return (
      <View>
        <ScrollView>
          {mapHash(this.state.remoteList, function (remote, index) {
            return (
              <RTCView
                mirror={true}
                key={index}
                streamURL={remote}
                style={styles.chefView}
              />
            );
          })}
        </ScrollView>
        <KeepAwake />

        <RTCView
          mirror={true}
          streamURL={this.state.selfViewSrc}
          style={styles.selfView}
        />

        <View
          style={{
            width: 50,
            height: 50,
            position: "absolute",
            backgroundColor: "transparent",
            top:
            Platform.OS === "ios"
              ? Dimensions.get("window").height - 50
              : ExtraDimensions.get("REAL_WINDOW_HEIGHT") -
              75 -
              ExtraDimensions.get("SOFT_MENU_BAR_HEIGHT")
          }}
        >
          <Icon
            name="stop-circle-o"
            onPress={this.props.closeBtn}
            size={50}
            color="red"
            style={{ height: 50, width: 50, margin: 2 }}
          />
        </View>
      </View>
    );
  }
};
// height: (Platform.OS === 'ios') ? 200 : 100,
// <Button onPress={this.props.closeBtn} title='' style={{width: 50, height: 50, backgroundColor: 'black'}}/>
// <View style={{width: 50, height: 50, backgroundColor: 'steelblue', position: 'absolute', top: Dimensions.get("window").height - 50}} />
// transform: [{ rotate: '90deg'}]
const styles = StyleSheet.create({
  selfViewOld: {
    zIndex: 1,
    position: "absolute",
    justifyContent: "center",
    left: 0,
    width: 150,
    height: 200,
    borderWidth: 2
    // transform: [{ scaleX: (Platform.OS === 'ios') ? -1 : 1 }],
  },
  selfView: {
    position: "absolute",
    top:
    Platform.OS === "ios" ? 0 : -ExtraDimensions.get("STATUS_BAR_HEIGHT") + 1,
    width: Dimensions.get("window").width / 3,
    height: Dimensions.get("window").height / 3,
    borderWidth: 2
    // transform: [{ scaleX: (Platform.OS === 'ios') ? -1 : 1 }],
  },
  chefView: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    // transform: [{ scaleX: (Platform.OS === 'ios') ? -1 : 1 }],
    backgroundColor: "black"
  },
  remoteView: {
    // alignSelf: 'flex-start' || 'flex-end' || 'center' || 'stretch' || 'auto',
    // justifyContent: 'center',
    width: 400,
    height: 300
  },
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  listViewContainer: {
    height: 150
  }
});

export default StudentsView;

// <ScrollView style={{flex:1}}>
//       <View style={styles.container}>
//         <Text style={styles.welcome}>
//           {this.state.info}
//         </Text>
//         {this.state.textRoomConnected && this._renderTextRoom()}
//         <View style={{flexDirection: 'row'}}>
//           <Text>
//             {this.state.isFront ? "Use front camera" : "Use back camera"}
//           </Text>
//           <TouchableHighlight
//             style={{borderWidth: 1, borderColor: 'black'}}
//             onPress={this._switchVideoType}>
//             <Text>Switch camera</Text>
//           </TouchableHighlight>
//         </View>
//         { this.state.status == 'ready' ?
//           (<View>
//             <TextInput
//               ref='roomID'
//               autoCorrect={false}
//               style={{width: 200, height: 40, borderColor: 'gray', borderWidth: 1}}
//               onChangeText={(text) => this.setState({roomID: text})}
//               value={this.state.roomID}
//             />
//             <TouchableHighlight
//               onPress={this._press}>
//               <Text>Enter room</Text>
//             </TouchableHighlight>
//           </View>) : null
//         }
//         <RTCView streamURL={this.state.selfViewSrc} style={styles.selfView}/>
//         {
//           mapHash(this.state.remoteList, function(remote, index) {
//             return <RTCView key={index} streamURL={remote} style={styles.remoteView}/>
//           })
//         }
//       <KeepAwake />
//       </View>
//       </ScrollView>
