// const SERVER_URL = 'http://192.168.1.65:9000'
const SERVER_URL = 'https://kitchenlive.tv'
const CLOUDINARY_API = '261525143558975'
const CLOUDINARY_USER = 'itstedpark'
const PRESET = 'tedPreset'
const DEFAULT_DOG_IMAGE = ``
const DEFAULT_PERSON_IMAGE = ``
const JANUS_SERVER = "ws://kitchenlive.co:8188"; //JANUS

export default {
    SERVER_URL,
    CLOUDINARY_USER,
    CLOUDINARY_API,
    PRESET,
    DEFAULT_DOG_IMAGE,
    DEFAULT_PERSON_IMAGE,
    JANUS_SERVER
};