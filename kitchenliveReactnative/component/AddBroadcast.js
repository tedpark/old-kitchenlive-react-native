import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  TouchableHighlight,
  AsyncStorage
} from "react-native";

import { Card, COLOR } from "react-native-material-ui";
import Icon from "react-native-vector-icons/FontAwesome";
import { TextField } from "react-native-material-textfield";

import serverUrl from "../config";
import {
  FormLabel,
  FormInput,
  FormValidationMessage,
  Button
} from "react-native-elements";
import DateTimePicker from "react-native-modal-datetime-picker";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

let styles = {};

const Viewport = Dimensions.get("window");

const uiTheme = {
  palette: {
    primaryColor: COLOR.green500
  },
  toolbar: {
    container: {
      height: 50
    }
  }
};

class AddBroadcast extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);

    this.state = {
      text: "",
      height: 0,
      isDatePickerVisible: false,
      dateTimeCooking: new Date()
    };
  }

  showDateTimePicker = () => this.setState({ isDatePickerVisible: true });

  hideDateTimePicker = () => this.setState({ isDatePickerVisible: false });

  handleDatePicked = date => {
    console.log("A date has been picked: ", date);
    this.setState({
      dateTimeCooking: date
    });
    this.hideDateTimePicker();
  };

  test = () => {
    // console.log(this.props.close)
    console.log(this.refs.formName.refs.name._lastNativeText);
    // console.log(this.refs.formName.refs.name)
  };

  fetchAddBroadcast = () => {
    AsyncStorage.getItem("meJson").then(me => {
      console.log(JSON.parse(me)._id);
      AsyncStorage.getItem("accessToken").then(token => {
        console.log("fetchAddBroadcast");
        fetch(serverUrl + "/api/broadcast-lists", {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token
          },
          body: JSON.stringify({
            userId: JSON.parse(me)._id,
            roomName: this.refs.formName.refs.name._lastNativeText,
            roomId:
              this.refs.formName.refs.name._lastNativeText +
              this.state.dateTimeCooking.toString(),
            broadcastDate: this.state.dateTimeCooking.toString(),
            roomDescription: this.refs.formDescription.refs.description
              ._lastNativeText,
            category: this.refs.formCategory.refs.category._lastNativeText,
            students: []
          })
        })
          .then(response => response.json())
          .then(responseData => {
            console.log("response object:", responseData);
            this.props.refresh();
            this.props.close();
          })
          .catch(error => {
            console.error(error);
          });
      });
    });
  };

  render() {
    return (
      <View style={{ backgroundColor: "#DDDDE2" }}>
        <View
          style={{
            marginTop: 0,
            height: 64,
            backgroundColor: "#EFEFF2",
            zIndex: 100
          }}
        >
          <View
            style={{
              flexDirection: "row",
              borderWidth: 0,
              height: 64,
              justifyContent: "space-between",
              alignItems: "center",
              marginTop: 10,
              borderBottomWidth: 0.8,
              borderColor: "#D9D9DF"
            }}
          >
            <Text
              style={{
                fontSize: 16,
                fontWeight: "bold",
                marginLeft: 10,
                color: "tomato"
              }}
              onPress={this.props.close}
            >
              Cancel
            </Text>
            <Text
              style={{ fontSize: 16, fontWeight: "bold", color: "seagreen" }}
              onPress={this.props.close}
            >
              요리 방송 추가
            </Text>
            <Text
              style={{
                fontSize: 16,
                fontWeight: "bold",
                marginRight: 10,
                color: "steelblue"
              }}
              onPress={() => {
                this.fetchAddBroadcast();
              }}
            >
              Save
            </Text>
          </View>
        </View>

        <Card>
          <KeyboardAwareScrollView>
            <FormLabel
              containerStyle={styles.labelContainerStyle}
              labelStyle={styles.labelStyle}
            >
              요리 이름
            </FormLabel>
            <FormInput
              ref="formName"
              textInputRef="name"
              multiline={true}
              placeholder="요리 이름을 입력해 주세요"
              inputStyle={styles.inputStyle}
            />

            <View style={{ marginBottom: 30 }} />

            <FormLabel
              labelStyle={{
                fontSize: 16,
                fontWeight: "bold",
                color: "rosybrown"
              }}
            >
              요리 날짜
            </FormLabel>
            <FormLabel
              labelStyle={{
                fontSize: 16,
                fontWeight: "bold",
                color: "seagreen"
              }}
            >
              {new Date(this.state.dateTimeCooking.toString()).getFullYear() +
                "년" +
                " " +
                (new Date(this.state.dateTimeCooking.toString()).getMonth() +
                  1) +
                "월" +
                " " +
                new Date(this.state.dateTimeCooking.toString()).getDate() +
                "일" +
                " " +
                new Date(this.state.dateTimeCooking.toString()).getHours() +
                "시" +
                " " +
                new Date(this.state.dateTimeCooking.toString()).getMinutes() +
                "분"}
            </FormLabel>

            <View style={{ marginBottom: 10 }} />

            <Button
              title="요리 날짜와 시간을 선택해주세요"
              raised
              buttonStyle={{ marginTop: 0 }}
              textStyle={{ fontSize: 14, fontWeight: "bold" }}
              backgroundColor="tomato"
              onPress={this.showDateTimePicker}
              borderRadius={2}
            />

            <View style={{ marginBottom: 30 }} />

            <FormLabel
              containerStyle={styles.labelContainerStyle}
              labelStyle={styles.labelStyle}
            >
              요리 카테고리
            </FormLabel>
            <FormInput
              ref="formCategory"
              textInputRef="category"
              multiline={false}
              placeholder="요리 카테고리를 입력해 주세요"
              inputStyle={styles.inputStyle}
            />

            <View style={{ marginBottom: 30 }} />

            <FormLabel
              containerStyle={styles.labelContainerStyle}
              labelStyle={styles.labelStyle}
            >
              URL Link
            </FormLabel>
            <FormInput
              ref="formLink"
              textInputRef="link"
              multiline={false}
              placeholder="URL Link를 입력해 주세요"
              inputStyle={styles.inputStyle}
            />

            <View style={{ marginBottom: 30 }} />

            <FormLabel
              containerStyle={styles.labelContainerStyle}
              labelStyle={styles.labelStyle}
            >
              요리 설명
            </FormLabel>
            <FormInput
              ref="formDescription"
              textInputRef="description"
              multiline={true}
              placeholder="요리 설명을 입력해 주세요"
              inputStyle={{
                height: 120,
                fontSize: 14,
                fontWeight: "bold",
                color: "darkslategrey"
              }}
            />
            <View style={{ marginBottom: 60 }} />
          </KeyboardAwareScrollView>
        </Card>

        <DateTimePicker
          mode="datetime"
          isVisible={this.state.isDatePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        />
      </View>
    );
  }
}

styles = StyleSheet.create({
  headingContainer: {
    marginTop: 60,
    justifyContent: "center",
    alignItems: "center",
    padding: 40,
    backgroundColor: COLOR.red100
  },
  heading: {
    color: "white",
    marginTop: 10,
    fontSize: 22
  },
  labelContainerStyle: {
    marginTop: 8
  },
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 20,
    paddingTop: 20
  },
  formLabel: {
    fontSize: 16,
    fontWeight: "bold"
  },
  labelStyle: {
    fontSize: 16,
    fontWeight: "bold",
    color: "seagreen"
  },
  inputStyle: {
    height: 40,
    fontSize: 14,
    fontWeight: "bold",
    color: "darkslategrey"
  }
});

export default AddBroadcast;

// <Text style={{fontSize: 20, fontWeight: 'bold'}} onPress={this.props.close}>Cancel</Text>
// <Icon name="podcast" onPress={() => navigate('AddBroadcast', { json: 'ss' })} size={28} color="red" style={{height: 28, width: 28, margin:10}}/>
