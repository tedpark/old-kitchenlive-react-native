import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  NativeModules,
  ScrollView,
  Modal,
  TouchableHighlight,
  Dimensions,
  FlatList,
  Image,
  AsyncStorage,
  Platform,
  Alert,
  Linking
} from "react-native";
const { InAppUtils } = NativeModules;
import Icon from "react-native-vector-icons/FontAwesome";
import { Card, COLOR, Checkbox } from "react-native-material-ui";
import StudentsViewJanus from "./StudentsViewJanus";
import { ListItem, Button, Tile, Divider } from "react-native-elements";
import axios from "axios";
const devServer = "http://192.168.0.102:9000";
const prodServer = "https://kitchenlive.tv";
const coServer = "https://kitchenlive.co";
var serverAddress = prodServer;
const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;
import serverUrl from "../config";

const uiTheme = {
  palette: {
    primaryColor: COLOR.green500
  },
  toolbar: {
    container: {
      height: 50
    }
  }
};

class InApp extends Component {
  static propTypes = {};

  static defaultProps = {};

  state = {};

  componentDidMount() {
    // this.buyOneMonthlyPremium()
  }

  buyOneMonthlyPremium = () => {
    var products = ["Monthlysubscription"];
    InAppUtils.loadProducts(products, (error, products) => {
      //update store here.
      console.log(products);

      var productIdentifier = "Monthlysubscription";
      InAppUtils.purchaseProduct(productIdentifier, (error, response) => {
        // NOTE for v3.0: User can cancel the payment which will be available as error object here.
        console.log(error);
        if (response && response.productIdentifier) {
          console.log(response);
          // Alert.alert('Purchase Successful', 'Your Transaction ID is ' + response.transactionIdentifier);
          //unlock store here.
        }
      });
    });
  };

  render() {
    return (
      <View
        style={{
          zIndex: -1,
          flex: 1,
          flexDirection: "column",
          justifyContent: "center",
          backgroundColor: "rgb(233, 233, 238)"
          // alignItems: 'center',
        }}
      >
        <View style={{ height: 20 }} />

        <Card>
          <Image
            style={{
              resizeMode: "contain",
              left: 120,
              width: Dimensions.get("window").width - 240,
              height: aspectRatio > 1.6 ? 60 : 120
            }}
            source={require("../image/Main_logo.png")}
          />

          <Text style={styles.labelDetailStyle}>
            1. 키친라이브의 프리미엄 서비스에 참여하시려면 {"\n"}
            구독 형태의 결제가 필요 합니다. (한달에 천원입니다.)
          </Text>

          <Text style={styles.labelDetailStyle}>
            2. 언제든 해지 가능한 구독 서비스 형태입니다.{"\n"}
            (설정 -> 아이튠즈 -> 구독에서 해지가능)
          </Text>

          <Image
            style={{
              resizeMode: "contain",
              left: 0,
              width: Dimensions.get("window").width - 20,
              height: aspectRatio > 1.6 ? 240 : 540
            }}
            source={require("../image/IapDetail.png")}
          />

          <Button
            raised
            backgroundColor="tomato"
            color="tomato"
            transparent={true}
            icon={{ name: "payment", color: "tomato" }}
            onPress={this.buyOneMonthlyPremium}
            textStyle={{ fontSize: 14, fontWeight: "bold" }}
            title="키친라이브 프리미엄 회원권 구독하기 0.9$"
            borderRadius={2}
          />

          <View style={{ height: aspectRatio > 1.6 ? 20 : 60 }} />

          <Button
            raised
            // backgroundColor='lightgray'
            color="tomato"
            transparent={true}
            icon={{ name: "cached", color: "tomato" }}
            // onPress={this.buyOneMonthlyPremium}
            textStyle={{ fontSize: 14, fontWeight: "bold" }}
            title="구매 내역 복원하기"
            borderRadius={2}
          />

          <View style={{ height: 20 }} />
        </Card>

        <ScrollView style={{ flex: 1 }}>
          <View style={{ flex: 1 }}>
            <Card>
              <Text
                style={[
                  styles.labelDetailStyle,
                  { color: "darkgray", lineHeight: 23 }
                ]}
              >
                유의사항: 위 표시 가격은 부가세 제외 금액입니다. {"\n"}
                구독료는 아이튠즈 계정을 통해서 구매가 확정된 시점부터{"\n"}
                청구되며, 앱스토어 정책상 구매자가 자동 갱신을 해제하지 {"\n"}
                않은 경우 기간 만료 후 24시간 이내에 자동 갱신되오니 {"\n"}
                유의하시기 바랍니다. 구매자는 계정 설정을 통하여 {"\n"}
                구독 구매 자동 갱신 여부를 사전에 해제할 수 있습니다. {"\n"}
                Notice: Payment will be charged to iTunes Account {"\n"}
                at confirmation of purchase. Subscription automatically{"\n"}
                renews unless auto-renew is turned off at {"\n"}
                least 24-hours before the end of the current period. {"\n"}
                Subscriptions may be managed by the user and {"\n"}
                auto-renewal may be turned off by going to the {"\n"}
                user's Account Settings after purchase. {"\n"}
                Privacy policy and Terms of use: {"\n"}
                <Text
                  style={[
                    styles.labelDetailStyle,
                    { color: "red", lineHeight: 23 }
                  ]}
                  onPress={() => {
                    Linking.openURL(
                      "https://kitchenlivetv.weebly.com/blog/kitchenlive"
                    );
                  }}
                >
                  Privacy Policy & Terms & Conditions
                </Text>
              </Text>
            </Card>
          </View>
        </ScrollView>

        {/* <Text>Inapppp11111111111111111111111</Text>
                <Text style={styles.labelDetailStyle}>
                    1. 키친라이브의 프리미엄 서비스는 {"\n"}
                    구독 형태의 결제가 필요 합니다. (한달 천원입니다.)
                          </Text>
                <Text style={styles.labelDetailStyle}>
                    2. 다양한 종류의 요리 영상 서비스 및 {"\n"}
                    프리미엄 서비스가 제공 됩니다.
                          </Text>
                <Text style={styles.labelDetailStyle}>
                    3. 언제든 해지 가능한 구독 서비스 형태입니다.{"\n"}
                    (설정->아이튠즈->구독에서 언제든 해지가능)
                          </Text>

                <View style={{ height: 40 }} /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headingContainer: {
    marginTop: 60,
    justifyContent: "center",
    alignItems: "center",
    padding: 40,
    backgroundColor: COLOR.red100
  },
  heading: {
    color: "white",
    marginTop: 10,
    fontSize: 22
  },
  labelContainerStyle: {
    marginTop: 8
  },
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 20,
    paddingTop: 20
  },
  formLabel: {
    fontSize: 16,
    fontWeight: "bold"
  },
  textStyle: {
    margin: 4,
    marginLeft: 8 + 0,
    textAlign: "left",
    fontSize: 14,
    fontWeight: "bold",
    color: "tomato"
  },
  labelStyle: {
    margin: 12,
    marginLeft: 12 + 6,
    textAlign: "center",
    fontSize: 14,
    fontWeight: "bold",
    color: "#424242"
  },
  labelDetailStyle: {
    margin: 4,
    // marginLeft: 12 + 6,
    textAlign: "center",
    fontSize: 14,
    fontWeight: "bold",
    color: "#424242",
    lineHeight: 27
  }
});

export default InApp;
