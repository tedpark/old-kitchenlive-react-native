import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Alert,
  AsyncStorage,
  Image
} from "react-native";
import { LoginManager } from "react-native-fbsdk";
import FBSDK from "react-native-fbsdk";
const { LoginButton, AccessToken } = FBSDK;
import {
  createStackNavigator,
  createBottomTabNavigator,
  TabNavigator,
  TabBarBottom,
  StackActions,
  NavigationActions
} from "react-navigation";
// import { Card, ListItem, Button, Tile, Divider } from "react-native-elements";
import serverUrl from "component/config";
import Dimensions from "Dimensions";
const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

class LoginView extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    AccessToken.getCurrentAccessToken().then(data => {
      if (data != null && data.accessToken != null) {
        // alert(data.accessToken.toString())
        console.log(data.accessToken.toString());
        this.fetchSignUpData(data.accessToken.toString());
        // this.props.navigation.navigate('MainListView', {name: 'Lucy'})
      }
    });
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "white"
        }}
      >
        <Image
          style={{
            width: aspectRatio > 1.6 ? Dimensions.get("window").width : 400,
            height: 400
          }}
          source={require("../image/k1024.png")}
        />

        <LoginButton
          style={{ width: Dimensions.get("window").width, height: 50 }}
          readPermissions={["public_profile"]}
          onLoginFinished={(error, result) => {
            if (error) {
              alert("Login failed with error: " + result.error);
            } else if (result.isCancelled) {
              alert("Login was cancelled");
            } else {
              AccessToken.getCurrentAccessToken().then(data => {
                if (data != null && data.accessToken != null) {
                  console.log(data.accessToken.toString());
                  {
                    /*AsyncStorage.setItem(
                    "accessToken",
                    data.accessToken.toString()
                  );
                  this.props.navigation.navigate("MainListView", {
                    name: "Lucy"
                  });*/
                  }
                  // LoginManager.logOut()
                  this.fetchSignUpData(data.accessToken.toString());
                  // this.props.navigation.navigate('LoginView', {name: 'Lucy'})
                } else {
                  this.fetchSignUpData(data.accessToken.toString());
                }
              });
              {
                /*this.props.navigation.navigate('MainListView', {name: 'Lucy'})*/
              }
            }
          }}
          onLogoutFinished={() => alert("User logged out")}
        />

        <View style={{ height: 70 }} />

        {/*<Button raised backgroundColor='green' onPress={() => navigate('MainListView', { json: 'ss' })} textStyle={{fontSize: 14, fontWeight: 'bold'}} title='goPass' borderRadius={2}/>*/}
      </View>
    );
  }

  fetchSignUpData = token => {
    console.log(serverUrl.SERVER_URL, token);
    fetch(serverUrl.SERVER_URL + "/auth/facebook-token?access_token=" + token, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        setTimeout(() => null, 0); // workaround for issue-6679
        return response.json();
      })
      .then(responseJson => {
        // return responseJson.movies;
        console.log(responseJson.kitchenlivetoken);
        AsyncStorage.setItem("accessToken", responseJson.kitchenlivetoken);
        this.fetchMeData(responseJson.kitchenlivetoken);
      })
      .catch(error => {
        console.error(error);
      });
  };

  fetchMeData = token => {
    fetch(serverUrl.SERVER_URL + "/api/users/me", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      }
    })
      .then(response => {
        setTimeout(() => null, 0); // workaround for issue-6679
        return response.json();
      })
      .then(responseJson => {
        // return responseJson.movies;
        console.log(responseJson);
        AsyncStorage.setItem("meJson", JSON.stringify(responseJson));

        // this.props.navigation.navigate("Root", {
        //   json: "this.state.loginView",
        //   token: token,
        //   userId: responseJson._id,
        //   meId: responseJson.userId,
        //   showAddButton: responseJson.userId === `5a44fe9e7e8d13001bdd298a` ? true : false,
        //   profileJson: responseJson
        // });

        const resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              routeName: "Root",
              params: {
                json: "this.state.loginView",
                token: token,
                userId: responseJson._id,
                meId: responseJson.userId,
                showAddButton:
                  responseJson.userId === `5a44fe9e7e8d13001bdd298a`
                    ? true
                    : false,
                profileJson: responseJson
              }
            })
          ]
        });
        this.props.navigation.dispatch(resetAction);
      })
      .catch(error => {
        console.error(error);
        this.props.navigation.navigate("SignUpView", {
          name: "SignUpView",
          token: token,
          userId: responseJson._id
        });
      });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
    // backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  }
});

export default LoginView;
