import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Platform,
  ScrollView,
  FlatList,
  TouchableWithoutFeedback,
  Modal,
  TouchableHighlight,
  TouchableOpacity,
  Dimensions,
  Image,
  AsyncStorage,
  SectionList
} from "react-native";

import { Card, COLOR } from "react-native-material-ui";
import { Container, Header, Item, Input, Icon } from "native-base";
import { StackNavigator, TabNavigator } from "react-navigation";
import { Divider, Button } from "react-native-elements";
import { TextField } from "react-native-material-textfield";
import {
  FormLabel,
  FormInput,
  ButtonGroup,
  FormValidationMessage,
  SocialIcon
} from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import serverUrl from "component/config";
const Viewport = Dimensions.get("window");
import DateTimePicker from "react-native-modal-datetime-picker";
import axios from "axios";

const uiTheme = {
  palette: {
    primaryColor: COLOR.green500
  },
  toolbar: {
    container: {
      height: 50
    }
  }
};

class EditProfileView extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);

    this.state = {
      name: "",
      kakaoId: "",
      address: "",
      cellPhone: "",
      profileImageUrl: [],
      cookingTime: String
    };
  }

  componentDidMount() {
    console.log(this.props.navigation.state.params.json);
    this.setState({
      name: this.props.navigation.state.params.json.name,
      kakaoId: this.props.navigation.state.params.json.kakaoId,
      address: this.props.navigation.state.params.json.address,
      cellPhone: this.props.navigation.state.params.json.cellPhone
    });
  }

  editSaveProfile = () => {
    AsyncStorage.getItem("accessToken").then(value => {
      let config = {
        Authorization: "Bearer " + value
      };
      axios
        .put(
          serverUrl.SERVER_URL +
            "/api/profiles/" +
            this.props.navigation.state.params.json._id,
          {
            name: this.state.name,
            kakaoId: this.state.kakaoId,
            address: this.state.address,
            cellPhone: this.state.cellPhone
          },
          config
        )
        .then(response => {
          console.log(response);
          this.props.navigation.state.params.refresh();
          this.props.navigation.goBack();
        })
        .catch(function(error) {
          console.log(error);
        });
    });
  };

  // name: String,
  // kakaoId: String,
  // address: String,
  // profileImageUrl: [String],

  render() {
    const buttons = ["여자", "남자"];
    return (
      <KeyboardAwareScrollView style={{ marginTop: 0 }}>
        <Card>
          <View style={{ flex: 1 }}>
            <View style={styles.container}>
              <TextField
                label="이름을 입력해 주세요"
                multiline={true}
                autoFocus={true}
                value={this.state.name}
                onChangeText={name => this.setState({ name })}
              />

              <TextField
                label="카카오ID를 입력해 주세요"
                multiline={true}
                value={this.state.kakaoId}
                onChangeText={kakaoId => this.setState({ kakaoId })}
              />

              <TextField
                label="전화번호를 입력해 주세요"
                multiline={true}
                value={this.state.cellPhone}
                onChangeText={cellPhone => this.setState({ cellPhone })}
              />

              <TextField
                label="주소를 입력해 주세요"
                multiline={true}
                value={this.state.address}
                onChangeText={address => this.setState({ address })}
              />
            </View>

            <View style={{ marginBottom: 20 }} />

            <Button
              title="수정하기"
              raised
              buttonStyle={{ marginTop: 0 }}
              textStyle={{ fontSize: 14, fontWeight: "bold" }}
              backgroundColor="green"
              onPress={() => this.editSaveProfile()}
              borderRadius={2}
            />
            <View style={{ marginBottom: 30 }} />
          </View>
        </Card>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  headingContainer: {
    marginTop: 60,
    justifyContent: "center",
    alignItems: "center",
    padding: 40,
    backgroundColor: COLOR.red100
  },
  container: {
    marginHorizontal: 12,
    marginVertical: 0,
    paddingHorizontal: 8
  },
  heading: {
    color: "white",
    marginTop: 10,
    fontSize: 22
  },
  labelContainerStyle: {
    marginTop: 8
  },

  formLabel: {
    fontSize: 16,
    fontWeight: "bold"
  }
});

export default EditProfileView;
