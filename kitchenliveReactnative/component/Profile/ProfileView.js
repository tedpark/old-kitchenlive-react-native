import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Platform,
  ScrollView,
  FlatList,
  TouchableWithoutFeedback,
  Modal,
  TouchableHighlight,
  TouchableOpacity,
  Dimensions,
  Image,
  AsyncStorage,
  SectionList
} from "react-native";

import { Card, COLOR } from "react-native-material-ui";
import { Container, Header, Item, Input, Icon, Button } from "native-base";
import { StackNavigator, TabNavigator } from "react-navigation";
import { Divider } from "react-native-elements";
import axios from "axios";
const listCellHeight = 120;
import serverUrl from "component/config";
// import RNFetchBlob from "react-native-fetch-blob";

const uiTheme = {
  palette: {
    primaryColor: COLOR.green500
  },
  toolbar: {
    container: {
      height: 50
    }
  }
};

class ProfileView extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);

    this.state = {
      refreshing: false,
      meJson: [],
      attendedCookingClass: [],
      preDogJson: [],
      uploadPersonURL: ``,
      uploadDogURL: ``,
      personAvatarSource: (
        <Image
          style={{ width: 100, height: 100, borderRadius: 50 }}
          source={{
            uri:
              "https://www.mariowiki.com/images/thumb/9/94/MushroomMarioKart8.png/200px-MushroomMarioKart8.png"
          }}
        />
      ),
      videoSource: null,
      imagePersonMain: null,
      imagePersonFirst: null,
      imagePersonSecond: null,
      imagePersonThird: null,
      imagePersonFourth: null,
      avatarImageDictionary: []
    };
  }

  static navigationOptions = {
    // title: "Kitchenlive.tv",
    headerTintColor: "tomato"
  };

  componentDidMount() {
    console.log(`profile`);
    AsyncStorage.getItem("meJson").then(value => {
      this.fetchMeData(JSON.parse(value)._id);
      this.fetchData(JSON.parse(value)._id);
      console.log(JSON.parse(value)._id);
      console.log(JSON.parse(value).facebook.id);

      this.setState({
        personAvatarSource: (
          <Image
            style={{ width: 100, height: 100, borderRadius: 50 }}
            source={{
              uri: `https://graph.facebook.com/${
                JSON.parse(value).facebook.id
              }/picture?type=large`
            }}
          />
        )
      });
    });
  }

  fetchMeData = userId => {
    AsyncStorage.getItem("accessToken").then(value => {
      //  this.setState({'data': value});
      fetch(serverUrl.SERVER_URL + "/api/profiles/" + userId, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Bearer " + value
        }
      })
        .then(response => {
          setTimeout(() => null, 0); // workaround for issue-6679
          return response.json();
        })
        .then(responseJson => {
          // this.setState({ meJson: [] }, () => {
          //   this.setState({
          //     meJson: responseJson
          //   });
          // });
          this.setState({
            meJson: [responseJson]
          });
          console.log(responseJson);
        })
        .catch(error => {
          console.log("Api call error");
          console.log(error);
        })
        .done();
      console.log(value);
    });
  };

  fetchData = userId => {
    AsyncStorage.getItem("accessToken").then(value => {
      // this.setState({ accessToken: value });
      fetch(serverUrl.SERVER_URL + "/api/broadcast-lists/attended/" + userId, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Bearer " + value
        }
      })
        .then(response => {
          setTimeout(() => null, 0); // workaround for issue-6679
          return response.json();
        })
        .then(responseJson => {
          // this.setState({ attendedCookingClass: [] }, () => {
          //   this.setState({
          //     attendedCookingClass: responseJson
          //   });
          // });
          this.setState({
            attendedCookingClass: responseJson
          });
          console.log(responseJson);
        })
        .catch(error => {
          console.log("Api call error");
          console.log(error);
        })
        .done();
      console.log(value);
    });
  };

  renderPersonItem = ({ item, index }) => (
    <Card>
      <TouchableWithoutFeedback
        onPress={() =>
          this.props.navigation.navigate("EditProfileView", {
            json: item,
            refresh: this.handleRefresh
            // refresh: this.close
          })
        }
      >
        <View>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "center",
                height: 170,
                marginLeft: 10
              }}
            >
              {this.state.personAvatarSource}
            </View>

            <View
              style={{
                flex: 2,
                justifyContent: "center",
                alignItems: "flex-start",
                height: 170,
                borderWidth: 0,
                borderColor: "lightgray",
                marginLeft: 10
              }}
            >
              <Text style={styles.contentLabel}>{item.name}</Text>
              <Text style={styles.contentLabel}>{item.kakaoId}</Text>
              <Text style={styles.contentLabel}>{item.cellPhone}</Text>
              <Text style={styles.contentLabel}>{item.address}</Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Card>
  );

  renderAttendedCooking = ({ item, index }) => (
    <Card>
      <TouchableWithoutFeedback
      // onPress={() =>
      //   navigate("MainListDetailView", {
      //     json: item,
      //     sss: navigate,
      //     index: index,
      //     refresh: this.fetchData
      //   })
      // }
      >
        <View>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "center",
                height: 120,
                marginLeft: 10
              }}
            >
              <Image
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 50
                }}
                source={{ uri: item.images[0] }}
              />
            </View>

            <View
              style={{
                flex: 2,
                justifyContent: "center",
                alignItems: "flex-start",
                height: 120,
                borderWidth: 0,
                borderColor: "lightgray",
                marginLeft: 10
              }}
            >
              <Text style={styles.contentLabel}>{item.roomName}</Text>
              <Text style={styles.contentLabel}>
                {new Date(item.broadcastDate).getFullYear() +
                  "년" +
                  " " +
                  (new Date(item.broadcastDate).getMonth() + 1) +
                  "월" +
                  " " +
                  new Date(item.broadcastDate).getDate() +
                  "일" +
                  " " +
                  new Date(item.broadcastDate).getHours() +
                  "시" +
                  " " +
                  new Date(item.broadcastDate).getMinutes() +
                  "분"}
              </Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Card>
  );

  handleRefresh = () => {
    AsyncStorage.getItem("meJson").then(value => {
      this.fetchMeData(JSON.parse(value)._id);
      this.fetchData(JSON.parse(value)._id);
      console.log(JSON.parse(value)._id);
    });
  };

  render() {
    return (
      <View style={{ height: Dimensions.get("window").height - 114 }}>
        <SectionList
          keyExtractor={(item, index) => index}
          removeClippedSubviews={false}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
          sections={[
            {
              data: this.state.meJson,
              key: "recently",
              title: "Recently",
              renderItem: this.renderPersonItem,
              extraData: this.state.meJson
            },
            {
              data: this.state.attendedCookingClass,
              key: "most_viewed",
              title: "Most viewed",
              renderItem: this.renderAttendedCooking,
              extraData: this.state.attendedCookingClass
            }
            // {
            //   data: ["walkaround"],
            //   key: "walkaround",
            //   title: "walkaround",
            //   renderItem: this.walkaround
            // },
            // {
            //   data: ["relationship"],
            //   key: "relationship",
            //   title: "relationship",
            //   renderItem: this.relationship
            // }
          ]}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: Platform.OS === "ios" ? 20 : 0
  },
  // container: {
  //   flex: 1,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   backgroundColor: '#F5FCFF',
  // },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  // contentLabel: {
  //   margin: 12,
  //   // marginLeft: 12 + 6,
  //   textAlign: "center",
  //   fontSize: 14,
  //   fontWeight: "bold",
  //   color: "seagreen"
  //   // color: "darkslategray"
  // },
  contentLabel: {
    fontSize: 14,
    fontWeight: "bold",
    margin: 5,
    padding: 5,
    color: "#424242",
    // backgroundColor: "ghostwhite",
    textAlign: "center"
  }
});

export default ProfileView;
