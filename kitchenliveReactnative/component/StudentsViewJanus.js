"use strict";

import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  TextInput,
  ListView,
  ScrollView,
  Platform,
  Dimensions,
  Button,
  PanResponder,
  Animated,
  Image,
  Alert
} from "react-native";

// import devServer from '../config'
import InCallManager from "react-native-incall-manager";
import KeepAwake from "react-native-keep-awake";
import Icon from "react-native-vector-icons/FontAwesome";
import ExtraDimensions from "react-native-extra-dimensions-android";

import {
  RTCPeerConnection,
  RTCMediaStream,
  RTCIceCandidate,
  RTCSessionDescription,
  RTCView,
  MediaStreamTrack,
  getUserMedia
} from "react-native-webrtc";

// const configuration = {
//   iceServers: [
//     {
//       urls: "turn:23.97.53.170:3478",
//       username: "ted",
//       credential: "park3764"
//     }
//   ]
// };

const pcPeers = {};
const bitrateGlobal = 128000 * 6;
let localStream;
let container;
var feeds = [];
const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

// const server = "ws://kitchenlive.tv:8188"; //JANUS
const server = "ws://webrtc.kitchenlive.tv:8188"; //JANUS
// const server = "ws://kitchenlive.co:8188"; //JANUS
// const server = "ws://192.168.1.65:8188"; //JANUS

import Janus from "./janus.nojquery.js";
// var Janus = require("./janus.nojquery.js");

var janus = null;
var sfutest = null;
var started = false;
var myusername = null;
var myid = null;
var mystream = null;
var feeds = [];
var bitrateTimer = [];
var localstream_janus, remotestream_janus;

function initStream() {
  // InCallManager.setMicrophoneMute(true);
  // InCallManager.track.enable = false
  InCallManager.start({ media: "video" });
  // InCallManager.setForceSpeakerphoneOn(true)
  // InCallManager.turnScreenOn.enable = true;
  // InCallManager.setKeepScreenOn.enable = true;
}

function logError(error) {
  console.log("logError", error);
}

function newRemoteFeed(id, display, roomId) {
  // A new feed has been published, create a new plugin handle and attach to it as a listener
  console.log("newRemoteFeed");
  console.log(roomId);
  console.log(id);
  console.log(display);
  var remoteFeed = null;
  janus.attach({
    plugin: "janus.plugin.videoroom",
    success(pluginHandle) {
      remoteFeed = pluginHandle;
      Janus.log(
        `Plugin attached! (${remoteFeed.getPlugin()}, id=${remoteFeed.getId()})`
      );
      Janus.log("  -- This is a subscriber");
      // We wait for the plugin to send us an offer1
      var listen = {
        request: "join",
        room: roomId,
        username: "club name",
        display: "displayzzzzzz",
        ptype: "listener",
        feed: id,
        videocodec: "h264",
        bitrate: bitrateGlobal
      };
      remoteFeed.send({ message: listen });
    },
    error(error) {
      Janus.error("  -- Error attaching plugin...", error);
      bootbox.alert(`Error attaching plugin... ${error}`);
    },
    onmessage(msg, jsep) {
      Janus.debug(" ::: Got a message (listener) :::");
      Janus.debug(JSON.stringify(msg));
      var event = msg.videoroom;
      Janus.debug(`Event: ${event}`);
      console.log(jsep);
      console.log(msg);
      console.log(event);
      if (event != undefined && event != null) {
        if (event === "attached") {
          // Subscriber created and attached
        }
      }
      if (jsep !== undefined && jsep !== null) {
        Janus.debug("Handling SDP as well...");
        Janus.debug(jsep);
        // Answer and attach
        remoteFeed.createAnswer({
          jsep: jsep,
          media: { audioSend: false, videoSend: false }, // We want recvonly audio/video
          success(jsep) {
            Janus.debug("Got SDP!");
            Janus.debug(jsep);
            var body = { request: "start", room: roomId };
            remoteFeed.send({ message: body, jsep });
          },
          error(error) {
            Janus.error("WebRTC error:", error);
            bootbox.alert(`WebRTC error... ${JSON.stringify(error)}`);
          }
        });
      }
    },
    webrtcState(on) {
      Janus.log(
        `Janus says this WebRTC PeerConnection (feed #${
          remoteFeed.rfindex
        }) is ${on ? "up" : "down"} now`
      );
    },
    onlocalstream(stream) {
      // The subscriber stream is recvonly, we don't expect anything here
      console.log("locals");
    },
    onremotestream(stream) {
      //#todo
      console.log(id);
      console.log(stream);
      console.log(display);
      console.log(stream.toURL());
      feeds.push([id, stream.toURL()]);
      /*feeds.append[id, stream.toURL()];*/
      console.log(feeds);
      console.log(feeds[0]);
      console.log(feeds[0][0]);
      container.setState({
        remotestreamArray: [
          ...container.state.remotestreamArray,
          [id, stream, display]
        ]
      });
      console.log(container.state.remotestreamArray);
    },
    oncleanup() {
      Janus.log(` ::: Got a cleanup notification (remote feed ${id}) :::`);
      if (remoteFeed.spinner !== undefined && remoteFeed.spinner !== null)
        remoteFeed.spinner.stop();
      remoteFeed.spinner = null;
    }
  });
}

class StudentsViewJanus extends Component {
  constructor(props) {
    super(props);
    this.panResponder = {};
    this.state = {
      info: "Initializing",
      status: "init",
      roomID: "",
      isFront: true,
      selfViewSrc: null,
      remoteTestSrc: null,
      remoteList: {},
      remotestreamArray: [],
      remotestreamArrayTest: [],
      textRoomConnected: false,
      textRoomData: [],
      textRoomValue: "",
      isPad: true
    };
  }

  componentWillMount() {
    // socket.connect();
    this.animatedvalue = new Animated.ValueXY({ x: 0.01, y: 0.01 });
    this._value = { x: 0, y: 0 };
    this.animatedvalue.addListener(value => (this._value = value));
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetResponderCapture: (evt, gestureState) => true,
      onPanResponderGrant: (e, gestureState) => {
        this.animatedvalue.setOffset({
          x: this._value.x,
          y: this._value.y
        });
        this.animatedvalue.setValue({ x: 0.01, y: 0.01 });
      },
      onPanResponderMove: Animated.event([
        null,
        { dx: this.animatedvalue.x, dy: this.animatedvalue.y }
      ])
    });
  }

  componentDidMount() {
    container = this;
    this.setState({ info: "Initializing", remotestreamArray: [] });
    this.setState({
      remotestreamArrayTest: [
        // [...container.state.remotestreamArray, [id, stream, display]]
        `id`,
        this.state.selfViewSrc,
        `display`,
        `id`,
        this.state.selfViewSrc,
        `display`,
        `id`,
        this.state.selfViewSrc,
        `display`,
        `id`,
        this.state.selfViewSrc,
        `display`,
        `id`,
        this.state.selfViewSrc,
        `display`,
        `id`,
        this.state.selfViewSrc,
        `display`
      ]
    });
    //incall manager remove
    // initStream();
    console.log(this.props.i);
    console.log(this.props.i.json.students); //이걸 가지고 방에 참가한 사람들 이름을 적거나, 프로필 사진을 띄울수 있겠군.
    console.log(this.props.i.json._id);
    console.log(this.props.userIdName);
    console.log(this.props.userId);
    console.log(this.props.meJson);
    console.log(
      new Date(
        parseInt(this.props.i.json._id.toString().substring(0, 8), 16) * 1000
      )
    );
    // console.log(this.props.i.json.broadcastDate);
    // console.log(new Date(this.props.i.json.broadcastDate).getMinutes());

    this.janusStart(
      // decodeURI(this.props.i.json.roomId)
      // 1234
      // this.props.i.json.roomId
      // new Date(this.props.i.json.broadcastDate)
      new Date(
        parseInt(this.props.i.json._id.toString().substring(0, 8), 16) * 1000
      ),
      this.props.userIdName + `&&&` + this.props.userId
    );
  }

  janusStart(roomId, userId) {
    console.log(roomId);
    console.log(userId);
    console.log(roomId.getFullYear());
    Janus.init({
      debug: "all",
      callback() {
        if (started) return;
        started = true;
      }
    });
    janus = new Janus({
      server,
      iceServers: [
        {
          url: "turn:23.97.53.170:3478",
          username: "ted",
          credential: "park3764"
        }
      ],
      success() {
        janus.attach({
          plugin: "janus.plugin.videoroom",
          success(pluginHandle) {
            sfutest = pluginHandle;
            Janus.log(
              `Plugin attached! (${sfutest.getPlugin()}, id=${sfutest.getId()})`
            );
            Janus.log("  -- This is a publisher/manager");
            sfutest.createOffer({
              // No media provided: by default, it's sendrecv for audio and video
              media: {
                audioRecv: false,
                videoRecv: false,
                audioSend: true,
                videoSend: true
              }, // Publishers are sendonly
              success(jsep) {
                Janus.debug("Got publisher SDP!");
                Janus.debug(jsep);
                var publish = {
                  request: "configure",
                  audio: true,
                  video: true,
                  bitrate: bitrateGlobal
                };
                sfutest.send({ message: publish, jsep: jsep });
              },
              error(error) {
                Janus.error("WebRTC error:", error);
              }
            });
            // join(this.props.i.json.roomId);
            console.log(roomId);
            // console.log(new Date(this.props.i.json.broadcastDate).getSeconds());
            console.log(
              roomId.getFullYear() *
                roomId.getMonth() *
                roomId.getDay() *
                roomId.getHours()
            );
            console.log(
              roomId.getFullYear() *
                roomId.getMilliseconds() *
                roomId.getMilliseconds() *
                roomId.getDay()
            );

            // var roomUniqueId =
            //   roomId.getFullYear() +
            //   roomId.getMonth() +
            //   roomId.getDay() +
            //   roomId.getHours() +
            //   roomId.getMinutes() +
            //   roomId.getSeconds();
            // console.log(roomUniqueId);

            const roomUniqueId = 1234

            var newRoom = {
              request: "create",
              room: roomUniqueId,
              ptype: "publisher",
              publishers: 6,
              bitrate: bitrateGlobal,
              videocodec: "h264"
            };
            // var newRoom = { "request": "create", "room": 567890, "ptype": "publisher", "is_private": "no", "publishers": 4 };
            sfutest.send({ message: newRoom });

            var register = {
              request: "join",
              room: roomUniqueId,
              ptype: "publisher",
              display: userId,
              videocodec: "h264"
            };
            sfutest.send({ message: register });

            console.log("send msg join room");
          },
          error(error) {
            Janus.error("  -- Error attaching plugin...", error);
          },
          consentDialog(on) {},
          mediaState(medium, on) {
            Janus.log(
              `Janus ${on ? "started" : "stopped"} receiving our a ${medium}`
            );
          },
          webrtcState(on) {
            Janus.log(
              `Janus says our WebRTC PeerConnection is ${
                on ? "up" : "down"
              } now`
            );
            // $("#videolocal").parent().parent().unblock();
          },
          onmessage(msg, jsep) {
            Janus.debug(" ::: Got a message (publisher) :::");
            Janus.debug(JSON.stringify(msg));
            var event = msg.videoroom;
            Janus.debug(`Event: ${event}`);
            if (event != undefined && event != null) {
              if (event === "joined") {
                // Publisher/manager created, negotiate WebRTC and attach to existing feeds, if any
                myid = msg.id;
                Janus.log(
                  `Successfully joined room ${msg.room} with ID ${myid}`
                );
                // publishOwnFeed(true);
                // Any new feed to attach to?
                if (msg.publishers !== undefined && msg.publishers !== null) {
                  var list = msg.publishers;
                  Janus.debug("Got a list of available publishers/feeds:");
                  Janus.debug(list);
                  for (var f in list) {
                    var id = list[f].id;
                    var display = list[f].display;
                    Janus.debug(`  >> [${id}] ${display}`);
                    var roomUniqueId =
                      roomId.getFullYear() +
                      roomId.getMonth() +
                      roomId.getDay() +
                      roomId.getHours() +
                      roomId.getMinutes() +
                      roomId.getSeconds();
                    newRemoteFeed(id, display, roomUniqueId);
                  }
                }
              } else if (event === "destroyed") {
                // The room has been destroyed
                Janus.warn("The room has been destroyed!");
              } else if (event === "event") {
                // Any new feed to attach to?
                if (msg.publishers !== undefined && msg.publishers !== null) {
                  var list = msg.publishers;
                  Janus.debug("Got a list of available publishers/feeds:");
                  Janus.debug(list);
                  for (var f in list) {
                    var id = list[f].id;
                    var display = list[f].display;
                    Janus.debug(`  >> [${id}] ${display}`);
                    var roomUniqueId =
                      roomId.getFullYear() +
                      roomId.getMonth() +
                      roomId.getDay() +
                      roomId.getHours() +
                      roomId.getMinutes() +
                      roomId.getSeconds();
                    newRemoteFeed(id, display, roomUniqueId);
                  }
                } else if (msg.leaving !== undefined && msg.leaving !== null) {
                  // One of the publishers has gone away?
                  var leaving = msg.leaving;
                  Janus.log(`Publisher left: ${leaving}`);
                  var remoteFeed = null;
                  container.setState({
                    remotestreamArray: container.state.remotestreamArray.filter(
                      function(person) {
                        return person[0] !== unpublished;
                      }
                    )
                  });
                } else if (
                  msg.unpublished !== undefined &&
                  msg.unpublished !== null
                ) {
                  // One of the publishers has unpublished?
                  var unpublished = msg.unpublished;
                  Janus.log(`Publisher left: ${unpublished}`);
                  console.log(msg.id);
                  console.log(msg);
                  // Todo
                  // this.setState(prevState => ({ people: prevState.people.filter(person => person !== e.target.value) }));

                  container.setState({
                    remotestreamArray: container.state.remotestreamArray.filter(
                      function(person) {
                        return person[0] !== unpublished;
                      }
                    )
                  });
                  console.log(container.state.remotestreamArray);

                  // this.setState(prevState => ({
                  //   remotestreamArray: prevState.remotestreamArray.filter(
                  //     person => person !== unpublished
                  //   )
                  // }));

                  if (unpublished === "ok") {
                    // That's us
                    sfutest.hangup();
                    return;
                  }
                } else if (msg.error !== undefined && msg.error !== null) {
                  // bootbox.alert(msg.error);
                }
              }
            }
            if (jsep !== undefined && jsep !== null) {
              Janus.debug("Handling SDP as well...");
              Janus.debug(jsep);
              sfutest.handleRemoteJsep({ jsep });
            }
          },
          onlocalstream(stream) {
            Janus.debug(stream);
            console.log(stream);
            // getLocalStream(true);
            container.setState({ selfViewSrc: stream.toURL() });
          },
          onremotestream(stream) {
            console.log("_orginalConnectJanus");
            // The publisher stream is sendonly, we don't expect anything here
          },
          oncleanup() {
            Janus.log(
              " ::: Got a cleanup notification: we are unpublished now :::"
            );
            mystream = null;
          }
        });
      },
      error(error) {
        // Janus.error(error);
        // bootbox.alert(error, function() {
        //     window.location.reload();
        // });
      },
      destroyed() {
        // window.location.reload();
        sfutest.send({ message: "unpublish" });
        // localstream_janus.release();
      }
    });
  }

  componentWillUnmount() {
    janus.destroy();
    if (localStream) {
      localStream.getTracks().forEach(t => {
        localStream.removeTrack(t);
      });
      localStream.release();
    }
  }

  _press(event) {
    this.refs.roomID.blur();
    this.setState({ status: "connect", info: "Connecting" });
    join(this.state.roomID);
  }

  _switchVideoType() {
    const isFront = !this.state.isFront;
    this.setState({ isFront });
    getLocalStream(isFront, function(stream) {
      if (localStream) {
        for (const id in pcPeers) {
          const pc = pcPeers[id];
          pc && pc.removeStream(localStream);
        }
        localStream.release();
      }
      localStream = stream;
      container.setState({ selfViewSrc: stream.toURL() });

      for (const id in pcPeers) {
        const pc = pcPeers[id];
        pc && pc.addStream(localStream);
      }
    });
  }

  publishOwnFeed(useAudio) {
    if (!this.state.publish) {
      this.setState({ publish: true });
      sfutest.createOffer({
        media: {
          audioRecv: false,
          videoRecv: false,
          audioSend: useAudio,
          videoSend: true
        },
        success: jsep => {
          var publish = { request: "configure", audio: useAudio, video: true };
          sfutest.send({ message: publish, jsep: jsep });
        },
        error: error => {
          Alert.alert("WebRTC error:", error);
          if (useAudio) {
            publishOwnFeed(false);
          } else {
          }
        }
      });
    } else {
      // this.setState({ publish: false });
      // let unpublish = { "request": "unpublish" };
      // sfutest.send({"message": unpublish});
    }
  }

  render() {
    const animatedStyle = {
      transform: this.animatedvalue.getTranslateTransform(),
      position: "absolute",
      zIndex: 1000000
    };

    return (
      <View style={{ flex: 1 }}>
        {this.state.remotestreamArray.map((remoteUrl, i) => {
          console.log(remoteUrl[2]);
          console.log(remoteUrl[2].split(`&&&`));
          //name
          console.log(remoteUrl[2].split(`&&&`)[0]);
          //_id
          console.log(remoteUrl[2].split(`&&&`)[1]);
          console.log(this.props.i.json.userId);
          console.log(this.props.userId);
          return (
            //chef는 메인 화면에 띄워주기
            this.props.i.json.chef === remoteUrl[2].split(`&&&`)[1] ? (
              <View key={i} style={{ position: "absolute", zIndex: 0 }}>
                <RTCView
                  mirror={true}
                  key={i}
                  streamURL={remoteUrl[1].toURL()}
                  style={{
                    width: Dimensions.get("window").width,
                    height: Dimensions.get("window").height,
                    // transform: [{scaleX: (Platform.OS === 'ios') ? -1 : 1 }],
                    backgroundColor: "black",
                    borderWidth: 0,
                    borderColor: "lightgray",
                    borderRadius: 8,
                    zIndex: 0
                  }}
                >
                  {Platform.OS === `ios` ? (
                    <View style={styles.userNameTag}>
                      <Text
                        style={{
                          fontWeight: "bold",
                          height: 20,
                          color: "red",
                          backgroundColor: "transparent"
                        }}
                      >
                        {remoteUrl[2].split(`&&&`)[0]}
                      </Text>
                    </View>
                  ) : null}
                </RTCView>
              </View>
            ) : null
          );
        })}
        {/* {aspectRatio > 1.6 ? ( */}
        {/* {aspectRatio > 1.6 && this.props.i.json.chef !== this.props.userId ? ( */}
        {aspectRatio > 1.6 && this.props.i.json.chef !== this.props.userId ? (
          //iPhone Students View Mode
          <ScrollView
            style={{
              position: "absolute",
              top: Dimensions.get("window").height / 2,
              width: Dimensions.get("window").width / 6,
              height: Dimensions.get("window").height / 2,
              backgroundColor: "transparent"
            }}
          >
            {this.state.remotestreamArray.map((remoteUrl, i) => {
              console.log(remoteUrl[2]);
              console.log(remoteUrl[2].split(`&&&`));
              //name
              console.log(remoteUrl[2].split(`&&&`)[0]);
              //_id
              console.log(remoteUrl[2].split(`&&&`)[1]);
              console.log(this.props.i.json._id);
              return this.props.i.json.chef ===
                remoteUrl[2].split(`&&&`)[1] ? null : (
                <View key={i}>
                  <RTCView
                    zOrder={2}
                    mirror={true}
                    key={i}
                    streamURL={remoteUrl[1].toURL()}
                    style={styles.chefView}
                  >
                    {/* {Platform.OS === `ios` ?
                        <View style={styles.userNameTag}>
                          <Text style={{
                            fontWeight: 'bold',
                            height: 30,
                            fontSize: 20,
                            color: 'red',
                            backgroundColor: 'transparent',
                          }} >{remoteUrl[2].split(`&&&`)[0]}</Text>
                        </View> : null} */}
                  </RTCView>
                </View>
              );
            })}
          </ScrollView>
        ) : (
          //iPad Mode
          <View
            style={{
              // flex: 1,
              flexDirection: "column",
              justifyContent: "space-between"
              // alignItems: 'center',
            }}
          >
            <View
              style={{
                // flex: 1,
                flexDirection: "row",
                justifyContent: "space-between"
              }}
            >
              {this.state.remotestreamArray.map((remoteUrl, i) => {
                // Chef View Mode
                return i <= 2 ? (
                  <View key={i}>
                    <RTCView
                      mirror={true}
                      key={i}
                      streamURL={remoteUrl[1].toURL()}
                      style={styles.chefModeView}
                    >
                      <View style={styles.userNameTagChefMode}>
                        <Text
                          style={{
                            fontWeight: "bold",
                            height: 30,
                            fontSize: 20,
                            color: "red",
                            backgroundColor: "transparent"
                          }}
                        >
                          {remoteUrl[2].split(`&&&`)[0]}
                        </Text>
                      </View>
                    </RTCView>
                  </View>
                ) : null;
              })}
            </View>

            <View
              style={{
                // flex: 1,
                flexDirection: "row",
                justifyContent: "space-between"
              }}
            >
              {this.state.remotestreamArray.map((remoteUrl, i) => {
                return i > 2 ? (
                  <View key={i}>
                    <RTCView
                      mirror={true}
                      key={i}
                      streamURL={remoteUrl[1].toURL()}
                      style={styles.chefModeView}
                    >
                      <View style={styles.userNameTagChefMode}>
                        <Text
                          style={{
                            fontWeight: "bold",
                            height: 20,
                            color: "red",
                            backgroundColor: "transparent"
                          }}
                        >
                          {remoteUrl[2].split(`&&&`)[0]}
                        </Text>
                      </View>
                    </RTCView>
                  </View>
                ) : null;
              })}
            </View>
          </View>
        )}

        <KeepAwake />

        {Platform.OS === "ios" ? (
          <Animated.View
            style={animatedStyle}
            {...this.panResponder.panHandlers}
          >
            <View style={{}}>
              <RTCView
                zOrder={2}
                mirror={true}
                streamURL={this.state.selfViewSrc}
                style={styles.selfView}
              >
                <View style={styles.userNameTagSelfView}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      height: 20,
                      color: "red",
                      backgroundColor: "transparent"
                    }}
                  >
                    {this.props.userIdName}
                  </Text>
                </View>
              </RTCView>
            </View>
          </Animated.View>
        ) : (
          <RTCView
            zOrder={2}
            mirror={true}
            streamURL={this.state.selfViewSrc}
            style={styles.selfView}
          />
        )}

        <View
          style={{
            width: 50,
            height: 50,
            position: "absolute",
            backgroundColor: "transparent",
            top:
              Platform.OS === "ios"
                ? Dimensions.get("window").height - 50
                : ExtraDimensions.get("REAL_WINDOW_HEIGHT") -
                  75 -
                  ExtraDimensions.get("SOFT_MENU_BAR_HEIGHT")
          }}
        >
          <Icon
            name="stop-circle-o"
            onPress={this.props.closeBtn}
            size={50}
            color="red"
            style={{ height: 50, width: 50, margin: 2 }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  selfViewOld: {
    zIndex: 1,
    position: "absolute",
    justifyContent: "center",
    left: 0,
    width: 150,
    height: 200,
    borderWidth: 2
    // transform: [{scaleX: (Platform.OS === 'ios') ? -1 : 1 }],
  },
  selfView: {
    position: "absolute",
    top:
      Platform.OS === "ios" ? 0 : -ExtraDimensions.get("STATUS_BAR_HEIGHT") + 1,
    width:
      aspectRatio > 1.6
        ? Dimensions.get("window").width / 3
        : Dimensions.get("window").width / 4,
    height: Dimensions.get("window").height / 3,
    backgroundColor: "black",
    borderRadius: 8

    // borderWidth: 0,
    // borderColor: "red"
    // transform: [{scaleX: (Platform.OS === 'ios') ? -1 : 1 }],
  },
  chefView: {
    // position: "absolute",
    width: Dimensions.get("window").width / 6,
    height: Dimensions.get("window").height / 6,
    // transform: [{scaleX: (Platform.OS === 'ios') ? -1 : 1 }],
    backgroundColor: "black",
    borderWidth: 0,
    borderColor: "lightgray",
    borderRadius: 8,
    zIndex: 2
  },
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  listViewContainer: {
    height: 150
  },
  chefModeView: {
    width: Dimensions.get("window").width / 3,
    height: Dimensions.get("window").height / 2,
    // transform: [{scaleX: (Platform.OS === 'ios') ? -1 : 1 }],
    backgroundColor: "black",
    borderRadius: 8,
    borderWidth: 0,
    borderColor: "red"
  },
  chefModeViewCenter: {
    width: Dimensions.get("window").width / 4,
    height: Dimensions.get("window").height / 3,
    // transform: [{scaleX: (Platform.OS === 'ios') ? -1 : 1 }],
    backgroundColor: "black",
    borderWidth: 0,
    borderColor: "lightgray"
  },
  userNameTagChefMode: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    right:
      aspectRatio > 1.6
        ? Dimensions.get("window").width / 4 / 2 - 10
        : Dimensions.get("window").width / 3 / 2 - 10,
    alignItems: "center",
    transform: [{ rotate: "-90deg" }]
  },
  userNameTagSelfView: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    right:
      aspectRatio > 1.6
        ? Dimensions.get("window").width / 3 / 2 - 10
        : Dimensions.get("window").width / 4 / 2 - 10,
    alignItems: "center",
    transform: [{ rotate: "-90deg" }]
  },
  userNameTag: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    right: Dimensions.get("window").width / 2 - 10,
    alignItems: "center",
    transform: [{ rotate: "-90deg" }]
  }
});

export default StudentsViewJanus;
