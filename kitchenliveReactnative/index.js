import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  AlertIOS,
  Button,
  Text,
  View,
  NativeModules,
  PushNotificationIOS,
  Platform,
  AsyncStorage,
  ActivityIndicator
} from "react-native";

import { LoginManager } from "react-native-fbsdk";
import FBSDK from "react-native-fbsdk";
const { LoginButton, AccessToken } = FBSDK;

import Icon from "react-native-vector-icons/FontAwesome";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Entypo from "react-native-vector-icons/Entypo";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import IconFoundation from "react-native-vector-icons/Foundation";
import {
  createStackNavigator,
  createBottomTabNavigator,
  TabNavigator,
  TabBarBottom,
  StackActions,
  NavigationActions
} from "react-navigation";
import { COLOR } from "react-native-material-ui";
import serverUrl from "./config";
import axios from "axios";

import MainListView from "./component/MainListView";
// import MainNavView from './component/MainNavView'
import MainListDetailView from "./component/MainListDetailView";
import LoginView from "./component/LoginView";
import AddBroadcast from "./component/AddBroadcast";
import SettingView from "./component/SettingView";
import SignUpView from "./component/SignUpView";

//ProfileView
import ProfileView from "Profile/ProfileView";
import EditProfileView from "Profile/EditProfileView";

import BuyCookGadget from "./component/BuyCookGadget";
import OffCookListView from "./component/OffCookListView";
import ChefListView from "./component/ChefListView";
// import BroadcastView from './component/BroadcastView.js'
// import ChefView from './component/ChefView'

const uiTheme = {
  palette: {
    primaryColor: COLOR.green500
  },
  toolbar: {
    container: {
      height: 50
    }
  }
};

class HomeView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accessToken: undefined,
      showAddButton: true
    };
    // this.props.navigation.navigate('Root', { name: 'Lucy' });
  }

  componentWillUnmount() {
    PushNotificationIOS.removeEventListener("register", this._onRegistered);
    PushNotificationIOS.removeEventListener(
      "registrationError",
      this._onRegistrationError
    );
    PushNotificationIOS.removeEventListener(
      "notification",
      this._onRemoteNotification
    );
    PushNotificationIOS.removeEventListener(
      "localNotification",
      this._onLocalNotification
    );
  }

  componentDidMount() {
    console.log(`go componentDidMount`);
    AccessToken.getCurrentAccessToken().then(data => {
      if (data != null && data.accessToken != null) {
        console.log(data.accessToken.toString());
        // this.checkSignUp(data.accessToken.toString());
        this.fetchSignUpData(data.accessToken.toString());
        // this.props.navigation.navigate('MainListView', {name: 'Lucy'})
      } else {
        const resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              routeName: "LoginView"
            })
          ]
        });
        this.props.navigation.dispatch(resetAction);

        // this.props.navigation.navigate("LoginView", { name: "Lucy" });
      }
    });
    // LoginManager.logOut()
    PushNotificationIOS.addEventListener("register", this._onRegistered);
    PushNotificationIOS.addEventListener(
      "registrationError",
      this._onRegistrationError
    );
    PushNotificationIOS.addEventListener(
      "notification",
      this._onRemoteNotification
    );
    PushNotificationIOS.addEventListener(
      "localNotification",
      this._onLocalNotification
    );
    if (Platform.OS === "ios") {
      PushNotificationIOS.requestPermissions();
    }
  }

  fetchSignUpData = token => {
    fetch(serverUrl + "/auth/facebook-token?access_token=" + token, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
        // 'Authorization': 'Bearer ' + token
      }
    })
      .then(response => {
        setTimeout(() => null, 0); // workaround for issue-6679
        return response.json();
      })
      .then(responseJson => {
        // return responseJson.movies;
        console.log(responseJson.kitchenlivetoken);
        AsyncStorage.setItem("accessToken", responseJson.kitchenlivetoken);
        this.setState({ accessToken: responseJson.kitchenlivetoken });
        this.fetchMeData(responseJson.kitchenlivetoken);
      })
      .catch(error => {
        console.error(error);
      });
  };

  fetchMeData = token => {
    fetch(serverUrl + "/api/users/me", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      }
    })
      .then(response => {
        setTimeout(() => null, 0); // workaround for issue-6679
        return response.json();
      })
      .then(responseJson => {
        // return responseJson.movies;
        console.log(responseJson);
        AsyncStorage.setItem("meJson", JSON.stringify(responseJson));

        // this.props.navigation.navigate("Root", {
        //   name: "Lucy",
        //   json: "this.state.loginView",
        //   token: token,
        //   userId: responseJson._id,
        //   profileJson: responseJson
        // });

        const resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              routeName: "Root",
              params: {
                name: "Lucy",
                json: "this.state.loginView",
                token: token,
                userId: responseJson._id,
                profileJson: responseJson
              }
            })
          ]
        });
        this.props.navigation.dispatch(resetAction);
      })
      .catch(error => {
        console.error(error);
      });
  };

  checkSignUp = token => {
    console.log("checkSignUp");
    console.log(token);

    AsyncStorage.getItem("meJson").then(value => {
      let userId = JSON.parse(value);
      console.log(userId._id);
      console.log(userId);

      let config = {
        Authorization: "Bearer " + token
      };
      axios
        .get(serverUrl + "/api/profiles/" + userId._id, config)
        .then(response => {
          setTimeout(() => null, 0); // workaround for issue-6679
          console.log(response);
          console.log(response.data.userId);

          switch (response.status) {
            case 401:
              this.props.navigation.navigate("LoginView", { name: "Lucy" });
              break;
          }
          // var show = true
          // if (this.props.navigation.state.params.meId === `5a44fe9e7e8d13001bdd298a`) {
          //   show = false
          // }

          if (response.data.userId === userId._id) {
            const resetAction = StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: "Root",
                  params: {
                    json: "this.state.loginView",
                    token: token,
                    userId: userId._id,
                    meId: response.data.userId,
                    showAddButton:
                      response.data.userId === `5a44fe9e7e8d13001bdd298a`
                        ? true
                        : false,
                    profileJson: response.data
                  }
                })
              ]
            });
            this.props.navigation.dispatch(resetAction);
          } else {
            this.props.navigation.navigate("SignUpView", {
              name: "Lucy",
              token: token,
              userId: userId._id
            });
          }
        })
        .catch(error => {
          console.log(error);
          this.props.navigation.navigate("SignUpView", {
            name: "Lucy",
            token: token,
            userId: userId._id
          });
        });
    });
  };

  fetchProfileData(userId, token) {
    console.log(userId, token);
    AsyncStorage.getItem("accessToken").then(value => {
      fetch(serverUrl + "/api/profiles/" + userId, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Bearer " + token
        }
      })
        .then(response => {
          setTimeout(() => null, 0); // workaround for issue-6679
          return response.json();
        })
        .then(responseJson => {
          console.log(responseJson);
          if (responseJson === undefined || responseJson === null) {
            this.props.navigation.navigate("SignUpView", { name: "Lucy" });
          } else {
            const resetAction = StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: "Root",
                  params: {
                    name: "Lucy",
                    json: "this.state.loginView",
                    token: token,
                    userId: userId,
                    profileJson: responseJson
                  }
                })
              ]
            });
            this.props.navigation.dispatch(resetAction);
          }
        })
        .catch(error => {
          console.error(error);
        });
    });
  }

  async getMoviesFromApi() {
    try {
      let response = await fetch(
        "https://facebook.github.io/react-native/movies.json"
      );
      let responseJson = await response.json();
      return responseJson.movies;
    } catch (error) {
      console.error(error);
    }
  }

  loginCheck = () => {
    var te = true;
    if (te) {
      // return <MainNavView/>
    } else {
      return <LoginView />;
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="tomato" />
      </View>
    );
  }

  _sendNotification() {
    require("RCTDeviceEventEmitter").emit("remoteNotificationReceived", {
      aps: {
        alert: "Sample notification",
        badge: "+1",
        sound: "default",
        category: "REACT_NATIVE"
      }
    });
  }

  _sendLocalNotification() {
    require("RCTDeviceEventEmitter").emit("localNotificationReceived", {
      aps: {
        alert: "Sample local notification",
        badge: "+1",
        sound: "default",
        category: "REACT_NATIVE"
      }
    });
  }

  _onRegistered(deviceToken) {
    console.log(deviceToken);
    // AlertIOS.alert(
    //   'Registered For Remote Push',
    //   `Device Token: ${deviceToken}`,
    //   [{
    //     text: 'Dismiss',
    //     onPress: null,
    //   }]
    // );
  }

  _onRegistrationError(error) {
    AlertIOS.alert(
      "Failed To Register For Remote Push",
      `Error (${error.code}): ${error.message}`,
      [
        {
          text: "Dismiss",
          onPress: null
        }
      ]
    );
  }

  _onRemoteNotification(notification) {
    AlertIOS.alert(
      "Push Notification Received",
      "Alert message: " + notification.getMessage(),
      [
        {
          text: "Dismiss",
          onPress: null
        }
      ]
    );
  }

  _onLocalNotification(notification) {
    AlertIOS.alert(
      "Local Notification Received",
      "Alert message: " + notification.getMessage(),
      [
        {
          text: "Dismiss",
          onPress: null
        }
      ]
    );
  }
}

// {this.loginCheck()}
// <BroadcastView/>
const TabNav = createBottomTabNavigator(
  {
    Home: {
      screen: MainListView,
      navigationOptions: ({ navigation, screenProps }) => ({
        title: Platform.OS === "ios" ? "Kitchen Live" : "Kitchen Live",
        headerTintColor: "tomato",
        headerBackTitle: null,
        gesturesEnabled: false,
        headerLeft: null,
        // headerLeft: (
        //   <Icon
        //     name="repeat"
        //     onPress={() => navigation.state.params.fetchData()}
        //     size={28}
        //     color="tomato"
        //     style={{ height: 34, width: 34, margin: 4 }}
        //   />
        // ),
        // headerLeft: <Icon name="navicon" onPress={() => navigation.navigate('SettingView', { json: "state" })} size={28} color="red" style={{height: 34, width: 34, margin:4}}/>,
        tabBarIcon: ({ tintColor, focused }) => (
          <Icon name={"home"} size={24} style={{ color: tintColor }} />
        )
      })
    },
    /*
    OffCookListViewTab: {
      screen: OffCookListView,
      navigationOptions: {
        title: Platform.OS === "ios" ? "OffLine" : "OffLine",
        headerTintColor: "tomato",
        headerLeft: null,
        tabBarIcon: ({ tintColor, focused }) => (
          <MaterialIcons
            name={"place"}
            size={24}
            style={{ color: tintColor }}
          />
        )
      }
    },
    */
    /*
    ChefListViewTab: {
      screen: ChefListView,
      navigationOptions: {
        title: Platform.OS === "ios" ? "ChefList" : "ChefList",
        headerTintColor: "tomato",
        headerLeft: null,
        tabBarIcon: ({ tintColor, focused }) => (
          <MaterialIcons
            name={"featured-play-list"}
            size={24}
            style={{ color: tintColor }}
          />
        )
      }
    },
    */
    /*
    BuyCookGadgetTab: {
      screen: BuyCookGadget,
      navigationOptions: {
        title: Platform.OS === "ios" ? "Cook Gadget" : "Cook Gadget",
        headerTintColor: "tomato",
        headerLeft: null,
        tabBarIcon: ({ tintColor, focused }) => (
          <MaterialCommunityIcons
            name={"food-variant"}
            size={24}
            style={{ color: tintColor }}
          />
        )
      }
    },
    */
    ProfileViewTab: {
      screen: ProfileView,
      navigationOptions: {
        title: Platform.OS === "ios" ? "Profile" : "Profile",
        headerLeft: null,
        tabBarIcon: ({ tintColor, focused }) => (
          <MaterialIcons
            name={"assignment-ind"}
            size={24}
            style={{ color: tintColor }}
          />
        )
      }
    }
  },
  {
    tabBarOptions: {
      activeTintColor: "tomato"
    }
  }
);

TabNav.navigationOptions = ({ navigation }) => {
  let { routeName } = navigation.state.routes[navigation.state.index];

  // You can do whatever you like here to pick the title based on the route name
  let headerTitle = routeName;

  if (headerTitle === "Home") {
    return {
      title: "KitchenLive",
      headerTintColor: "tomato"
      /*
      headerRight: (
        <Icon
          name="barcode"
          onPress={() => {
            console.log(navigation);
            navigation.navigate("SearchBooksView", {
              sharebookstoken: navigation.state.params.sharebookstoken,
              meJson: navigation.state.params.meJson,
              refresh: navigation.state.params.refresh
            });
            // navigation.state.params.setModalVisible(
            //   navigation.state.params.modalModalViewVisible
            // );
          }}
          size={38}
          color="darkgray"
          borderRadius={5}
          style={{ marginRight: 10, marginTop: 4 }}
        />
      )
      */
    };
  } else if (headerTitle === "ProfileViewTab") {
    return {
      title: "Profile",
      headerTintColor: "tomato"
    };
  } else {
    return {};
  }
};

// import ProfileView from "./component/ProfileView";
// import BuyCookGadget from "./component/BuyCookGadget";
// import OffCookListView from "./component/OffCookListView";
// import ChefListView from "./component/ChefListView";

const kitchenliveReactnative = createStackNavigator({
  HomeView: { screen: HomeView },
  LoginView: { screen: LoginView },
  SignUpView: { screen: SignUpView },
  Root: {
    screen: TabNav,
    navigationOptions: {
      headerBackTitle: null,
      gesturesEnabled: false
    }
  },
  MainListView: { screen: MainListView },
  MainListDetailView: { screen: MainListDetailView },
  AddBroadcast: { screen: AddBroadcast },
  SettingView: { screen: SettingView },
  EditProfileView: { screen: EditProfileView }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
    // backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});

AppRegistry.registerComponent(
  "kitchenliveReactnative",
  () => kitchenliveReactnative
);
